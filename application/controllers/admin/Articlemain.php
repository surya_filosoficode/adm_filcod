<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articlemain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        $this->load->library("Time_master");
        // $this->auth_v0->check_session_active_ad();
    }
 

    public function index(){
        $data["page"] = "article_main";
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        $data["list_jenis"] = $this->mm->get_data_all_where("article_jenis", []);
        $data["list_tipe"] = $this->mm->get_data_all_where("article_tipe", []);
        
        $this->load->view('index', $data);
    }

    public function index_list(){
        $data["page"] = "article_list";
        $data["list_article"] = $this->mm->get_data_all_where("article_main", ["is_delete_article"=>"0"]);

        $this->load->view('index', $data);
    }

    public function index_update($id_article_main = ""){
        $data["page"] = "article_update";

        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        $data["list_jenis"] = $this->mm->get_data_all_where("article_jenis", []);
        $data["list_tipe"] = $this->mm->get_data_all_where("article_tipe", []);

        $data["list_article"] = $this->mm->get_data_each("article_main", ["id_article_main"=>$id_article_main, "is_delete_article"=>"0"]);
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function val_save(){
        $config_val_input = array(
                array(
                    'field'=>'title_article',
                    'label'=>'title_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tipe_article',
                    'label'=>'tipe_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'category_article',
                    'label'=>'category_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tag_article',
                    'label'=>'tag_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'main_img_article',
                    'label'=>'main_img_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'content_article',
                    'label'=>'content_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "title_article"=>"",
                    "tipe_article"=>"",
                    "category_article"=>"",
                    "tag_article"=>"",
                    "main_img_article"=>"",
                    "content_article"=>""
                );

        if($this->val_save()){
            $title_article     = $this->input->post("title_article", true);
            $tipe_article     = $this->input->post("tipe_article", true);
            $category_article  = $this->input->post("category_article", true);
            $tag_article       = $this->input->post("tag_article", true);
            $main_img_article  = $this->input->post("main_img_article", true);
            $content_article   = $this->input->post("content_article");

            $create_date_article    = date("Y-m-d H:i:s");
            $create_admin_article   = $_SESSION["ih_mau_ngapain"]["id_admin"];

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, "ok"]];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $main_img_article_fix = str_replace(base_url(), "base_url/", $main_img_article);
                $content_article_fix = str_replace(base_url(), "base_url/", $content_article);

                $data = ["id_article_main"=>"",
                        "title_article"=>$title_article,
                        "tipe_article"=>$tipe_article,
                        "category_article"=>$category_article,
                        "tag_article"=>$tag_article,
                        "main_img_article"=>$main_img_article_fix,
                        "content_article"=>$content_article_fix,
                        "create_admin_article"=>$create_admin_article,
                        "create_date_article"=>$create_date_article,
                        "status_check_article"=>"0",
                        "admin_check_article"=>"0",
                        "date_check_article"=>"0",
                        "status_post_article"=>"0",
                        "admin_post_article"=>"0",
                        "date_post_article"=>"0",
                        "is_delete_article"=>"0"
                    ];

                // print_r($data);

                $insert = $this->mm->insert_data("article_main", $data);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["title_article"]    = strip_tags(form_error('title_article'));
            $msg_detail["tipe_article"]     = strip_tags(form_error('tipe_article'));
            $msg_detail["category_article"] = strip_tags(form_error('category_article'));
            $msg_detail["tag_article"]      = strip_tags(form_error('tag_article'));
            $msg_detail["main_img_article"] = strip_tags(form_error('main_img_article'));
            $msg_detail["content_article"]  = strip_tags(form_error('content_article'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_article_main"])){
            $id_article_main = $this->input->post('id_article_main');
            $data = $this->mm->get_data_each("article_main", array("id_article_main"=>$id_article_main, "is_delete_article"=>"0"));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_update(){
        $config_val_input = array(
                array(
                    'field'=>'id_article_main',
                    'label'=>'id_article_main',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'title_article',
                    'label'=>'title_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tipe_article',
                    'label'=>'tipe_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'category_article',
                    'label'=>'category_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tag_article',
                    'label'=>'tag_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'main_img_article',
                    'label'=>'main_img_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'content_article',
                    'label'=>'content_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_article_main"=>"",
                    "title_article"=>"",
                    "tipe_article"=>"",
                    "category_article"=>"",
                    "tag_article"=>"",
                    "main_img_article"=>"",
                    "content_article"=>""
                );

        if($this->val_update()){
            $id_article_main   = $this->input->post("id_article_main", true);

            $title_article     = $this->input->post("title_article", true);
            $tipe_article      = $this->input->post("tipe_article", true);
            $category_article  = $this->input->post("category_article", true);
            $tag_article       = $this->input->post("tag_article", true);
            $main_img_article  = $this->input->post("main_img_article", true);
            $content_article   = $this->input->post("content_article");

            $create_date_article    = date("Y-m-d H:i:s");
            $create_admin_article   = $_SESSION["ih_mau_ngapain"]["id_admin"];

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_article_main]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $main_img_article_fix = str_replace(base_url(), "base_url/", $main_img_article);
                $content_article_fix = str_replace(base_url(), "base_url/", $content_article);
                
                $data = ["title_article"=>$title_article,
                         "tipe_article"=>$tipe_article,
                         "category_article"=>$category_article,
                         "tag_article"=>$tag_article,
                         "main_img_article"=>$main_img_article_fix,
                         "content_article"=>$content_article_fix
                        ];

                $where = ["id_article_main"=>$id_article_main];

                $update = $this->mm->update_data("article_main", $data, $where);

                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else{
            $msg_detail["id_article_main"]  = strip_tags(form_error('id_article_main'));
            $msg_detail["title_article"]    = strip_tags(form_error('title_article'));
            $msg_detail["tipe_article"]     = strip_tags(form_error('tipe_article'));
            $msg_detail["category_article"] = strip_tags(form_error('category_article'));
            $msg_detail["tag_article"]      = strip_tags(form_error('tag_article'));
            $msg_detail["main_img_article"] = strip_tags(form_error('main_img_article'));
            $msg_detail["content_article"]  = strip_tags(form_error('content_article'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_article_main"=>"",
                );

        if($_POST["id_article_main"]){
            $id_article_main = $this->input->post("id_article_main", true);

            $set = array("is_delete_article"=>"1");
            $where = array("id_article_main"=>$id_article_main);

            // $delete_toko = $this->mm->delete_data("toko", array("id_article_main"=>$id_article_main));
            $delete_toko = $this->mm->update_data("article_main", $set, $where);
            
            if($delete_toko){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_article_main"]= strip_tags(form_error('id_article_main'));        
        }

        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function check($param = "0"){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_article_main"=>"",
                );

        if($_POST["id_article_main"]){
            $id_article_main = $this->input->post("id_article_main", true);

            $date    = date("Y-m-d H:i:s");
            $admin   = $_SESSION["ih_mau_ngapain"]["id_admin"];

            $set = array("status_check_article"=>$param,
                        "admin_check_article"=>$admin,
                        "date_check_article"=>$date
                        );

            $where = array("id_article_main"=>$id_article_main);

            // $delete_toko = $this->mm->delete_data("toko", array("id_article_main"=>$id_article_main));
            $delete_toko = $this->mm->update_data("article_main", $set, $where);
            
            if($delete_toko){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_article_main"]= strip_tags(form_error('id_article_main'));        
        }

        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }



     public function post($param = "0"){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_article_main"=>"",
                );

        if($_POST["id_article_main"]){
            $id_article_main = $this->input->post("id_article_main", true);

            $date    = date("Y-m-d H:i:s");
            $admin   = $_SESSION["ih_mau_ngapain"]["id_admin"];

            $set = array("status_post_article"=>$param,
                        "admin_post_article"=>$admin,
                        "date_post_article"=>$date
                        );

            $where = array("id_article_main"=>$id_article_main);

            // $delete_toko = $this->mm->delete_data("toko", array("id_article_main"=>$id_article_main));
            $delete_toko = $this->mm->update_data("article_main", $set, $where);
            
            if($delete_toko){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_article_main"]= strip_tags(form_error('id_article_main'));        
        }

        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


}
