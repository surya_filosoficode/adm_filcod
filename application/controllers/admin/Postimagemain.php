<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Postimagemain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        date_default_timezone_set("Asia/Bangkok");
    }

    public function index(){
        $data["page"] = "article_main";
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        
        $this->load->view('index', $data);
    }

    public function val_save(){
        $config_val_input = array(
                array(
                    'field'=>'article',
                    'label'=>'article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tag',
                    'label'=>'tag',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'cate',
                    'label'=>'cate',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save(){
        if($this->val_save_article()){
            $article = $this->input->post("article");
            print_r($_POST);
            // print_r($article);
            // print_r("ok");

            // $insert = $this->db->query("SELECT insert_main_article('surya', '".$article."') AS id_main_article")->row_array();

            // if($insert){
            //     print_r($insert);
            // }
        }
    }


    public function get(){
        $data = $this->db->get_where("main_article", [])->result();

        foreach ($data as $key => $value) {
            print_r($value->main_article);
        }
        // print_r($data);
    }

}
