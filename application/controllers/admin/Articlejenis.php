<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articlejenis extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        $this->load->library("magic_pattern");

        date_default_timezone_set("Asia/Bangkok");
    }

    public function index(){
        $data["page"] = "article_jenis";
        $data["list_data"] = $this->mm->get_data_all_where("article_jenis", []);

        $this->load->view("index", $data);
    }

    public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'nama_jenis_article',
                    'label'=>'nama_jenis_article',
                    'rules'=>'required|is_unique[article_jenis.nama_jenis_article]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'parent_jenis_article',
                    'label'=>'parent_jenis_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "nama_jenis_article"=>"",
                    "parent_jenis_article"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_insert()){
            $nama_jenis_article     = strtolower($this->input->post("nama_jenis_article"));
            $parent_jenis_article   = $this->input->post("parent_jenis_article");
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nama_jenis_article]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $data = ["id_jenis_article"     => "",
                         "nama_jenis_article"   => $nama_jenis_article,
                         "parent_jenis_article"    => $parent_jenis_article,
                         "is_delete_jenis_article"=> "0"];

                $insert = $this->mm->insert_data("article_jenis", $data);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["nama_jenis_article"]= strip_tags(form_error('nama_jenis_article'));
            $msg_detail["parent_jenis_article"]= strip_tags(form_error('parent_jenis_article'));
        }


        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_jenis_article"])){
            $id_jenis_article = $this->input->post('id_jenis_article');
            $data = $this->mm->get_data_each("article_jenis", array("id_jenis_article"=>$id_jenis_article));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis_article',
                    'label'=>'id_jenis_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'nama_jenis_article',
                    'label'=>'nama_jenis_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'parent_jenis_article',
                    'label'=>'parent_jenis_article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "id_jenis_article"=>"",
                    "nama_jenis_article"=>"",
                    "parent_jenis_article"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_update()){
            $id_jenis_article       = $this->input->post("id_jenis_article");

            $nama_jenis_article     = strtolower($this->input->post("nama_jenis_article"));
            $parent_jenis_article   = $this->input->post("parent_jenis_article");
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_jenis_article],
                            [$type_pattern, $nama_jenis_article],
                            [$type_pattern, $parent_jenis_article]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $where = ["id_jenis_article"=> $id_jenis_article];

                $data = ["nama_jenis_article"=> $nama_jenis_article,
                         "parent_jenis_article"=> $parent_jenis_article];

                $check_category = $this->mm->get_data_each("article_jenis", ["nama_jenis_article"=>$nama_jenis_article, "id_jenis_article!="=>$id_jenis_article]);
                if($check_category){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }else{
                    $insert = $this->mm->update_data("article_jenis", $data, $where);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_detail["id_jenis_article"]     = strip_tags(form_error('id_jenis_article'));
            $msg_detail["nama_jenis_article"]   = strip_tags(form_error('nama_jenis_article'));
            $msg_detail["parent_jenis_article"] = strip_tags(form_error('parent_jenis_article'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array(
                    "id_jenis_article"=>"");
        if(isset($_POST['id_jenis_article'])){
            $id_jenis_article = $this->input->post("id_jenis_article", true);
            
                
            if($this->mm->delete_data("article_jenis", ["id_jenis_article"=>$id_jenis_article])){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

}
