<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productbrand extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        $this->load->library("magic_pattern");

        date_default_timezone_set("Asia/Bangkok");
    }

    public function index(){
        $data["page"] = "product_brand";
        $data["list_data"] = $this->mm->get_data_all_where("product_brand", []);

        $this->load->view("index", $data);
    }

    public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'nama_brand',
                    'label'=>'nama_brand',
                    'rules'=>'required|is_unique[product_brand.nama_brand]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "nama_brand"=>"");

        if($this->val_form_insert()){
            $nama_brand     = strtolower($this->input->post("nama_brand"));

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nama_brand]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $data = ["id_brand"     => "",
                         "nama_brand"   => $nama_brand,
                         "is_delete_brand"=> "0"];

                $insert = $this->mm->insert_data("product_brand", $data);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["nama_brand"]= strip_tags(form_error('nama_brand'));
        }


        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_brand"])){
            $id_brand = $this->input->post('id_brand');
            $data = $this->mm->get_data_each("product_brand", array("id_brand"=>$id_brand));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'id_brand',
                    'label'=>'id_brand',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'nama_brand',
                    'label'=>'nama_brand',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "id_brand"=>"",
                    "nama_brand"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_update()){
            $id_brand       = $this->input->post("id_brand");

            $nama_brand     = strtolower($this->input->post("nama_brand"));
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_brand],
                            [$type_pattern, $nama_brand]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $where = ["id_brand"=> $id_brand];

                $data = ["nama_brand"=> $nama_brand];

                $check_category = $this->mm->get_data_each("product_brand", ["nama_brand"=>$nama_brand, "id_brand!="=>$id_brand]);
                if($check_category){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }else{
                    $insert = $this->mm->update_data("product_brand", $data, $where);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_detail["id_brand"]     = strip_tags(form_error('id_brand'));
            $msg_detail["nama_brand"]   = strip_tags(form_error('nama_brand'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array(
                    "id_brand"=>"");
        if(isset($_POST['id_brand'])){
            $id_brand = $this->input->post("id_brand", true);
            
                
            if($this->mm->delete_data("product_brand", ["id_brand"=>$id_brand])){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

}
