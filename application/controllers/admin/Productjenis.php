<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productjenis extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        $this->load->library("magic_pattern");

        date_default_timezone_set("Asia/Bangkok");
    }

    public function index(){
        $data["page"] = "product_jenis";
        $data["list_data"] = $this->mm->get_data_all_where("product_jenis", []);

        $this->load->view("index", $data);
    }

    public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'nama_jenis',
                    'label'=>'nama_jenis',
                    'rules'=>'required|is_unique[product_jenis.nama_jenis]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'parent_jenis',
                    'label'=>'parent_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "nama_jenis"=>"",
                    "parent_jenis"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_insert()){
            $nama_jenis     = $this->input->post("nama_jenis");
            $parent_jenis   = $this->input->post("parent_jenis");
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nama_jenis]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $data = ["id_jenis"     => "",
                         "nama_jenis"   => $nama_jenis,
                         "parent_id"    => $parent_jenis,
                         "is_delete_jenis"=> "0"];

                $insert = $this->mm->insert_data("product_jenis", $data);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["nama_jenis"]= strip_tags(form_error('nama_jenis'));
            $msg_detail["parent_jenis"]= strip_tags(form_error('parent_jenis'));
        }


        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_jenis"])){
            $id_jenis = $this->input->post('id_jenis');
            $data = $this->mm->get_data_each("product_jenis", array("id_jenis"=>$id_jenis));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'id_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'nama_jenis',
                    'label'=>'nama_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'parent_jenis',
                    'label'=>'parent_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "id_jenis"=>"",
                    "nama_jenis"=>"",
                    "parent_jenis"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_update()){
            $id_jenis       = $this->input->post("id_jenis");

            $nama_jenis     = $this->input->post("nama_jenis");
            $parent_jenis   = $this->input->post("parent_jenis");
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_jenis],
                            [$type_pattern, $nama_jenis],
                            [$type_pattern, $parent_jenis]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $where = ["id_jenis"=> $id_jenis];

                $data = ["nama_jenis"=> $nama_jenis,
                         "parent_id"=> $parent_jenis];

                $check_category = $this->mm->get_data_each("product_jenis", ["nama_jenis"=>$nama_jenis, "id_jenis!="=>$id_jenis]);
                if($check_category){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }else{
                    $insert = $this->mm->update_data("product_jenis", $data, $where);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_detail["id_jenis"]     = strip_tags(form_error('id_jenis'));
            $msg_detail["nama_jenis"]   = strip_tags(form_error('nama_jenis'));
            $msg_detail["parent_jenis"] = strip_tags(form_error('parent_jenis'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array(
                    "id_jenis"=>"");
        if(isset($_POST['id_jenis'])){
            $id_jenis = $this->input->post("id_jenis", true);
            
                
            if($this->mm->delete_data("product_jenis", ["id_jenis"=>$id_jenis])){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

}
