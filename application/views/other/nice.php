
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>assets/template/assets/images/favicon.png">
    <title>Admin Press Admin Template - The Ultimate Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Cropper CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/cropper/cropper.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/colors/blue.css" id="theme" rel="stylesheet">

    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        
        
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Image cropper</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item">Forms</li>
                        <li class="breadcrumb-item active">Image cropper</li>
                    </ol>
                </div>
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <!-- .Your image -->
                                    <div class="col-md-12 p-20">
                                        <div class="img-container"><img id="image" src="<?php print_r(base_url());?>assets/template/assets/images/big/img2.jpg" class="img-responsive" alt="Picture"></div>
                                    </div>
                                    <!-- /.Your image -->
                                    <!-- .Croping image -->
                                    <div class="col-md-12 p-20">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="docs-preview clearfix">
                                                    <div class="img-preview" style="width: 358px; height: 231px;"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-12 docs-buttons">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info" data-method="setDragMode" data-option="move" title="Move"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setDragMode&quot;, &quot;move&quot;)"> <span class="fa fa-arrows"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-info" data-method="setDragMode" data-option="crop" title="Crop"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setDragMode&quot;, &quot;crop&quot;)"> <span class="fa fa-crop"></span> </span>
                                                            </button>
                                                        </div>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-success" data-method="zoom" data-option="0.1" title="Zoom In"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, 0.1)"> <span class="fa fa-search-plus"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-success" data-method="zoom" data-option="-0.1" title="Zoom Out"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, -0.1)"> <span class="fa fa-search-minus"></span> </span>
                                                            </button>
                                                        </div>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="-10" data-second-option="0" title="Move Left"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, -10, 0)"> <span class="fa fa-arrow-left"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="10" data-second-option="0" title="Move Right"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 10, 0)"> <span class="fa fa-arrow-right"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="0" data-second-option="-10" title="Move Up"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 0, -10)"> <span class="fa fa-arrow-up"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="0" data-second-option="10" title="Move Down"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 0, 10)"> <span class="fa fa-arrow-down"></span> </span>
                                                            </button>
                                                        </div>

                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="scaleX" data-option="-1" title="Flip Horizontal"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;scaleX&quot;, -1)"> <span class="fa fa-arrows-h"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="scaleY" data-option="-1" title="Flip Vertical"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;scaleY&quot;, -1)"> <span class="fa fa-arrows-v"></span> </span>
                                                            </button>
                                                        </div>

                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="rotate" data-option="-45" title="Rotate Left"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, -45)"> <span class="fa fa-rotate-left"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="rotate" data-option="45" title="Rotate Right"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, 45)"> <span class="fa fa-rotate-right"></span> </span>
                                                            </button>
                                                        </div>

                                                        <div class="btn-group btn-group-crop">
                                                            <button style="width: 45px;" type="button" class="btn btn-secondary btn-warning" data-method="reset" title="Reset"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;reset&quot;)"> <span class="fa fa-refresh"></span> </span>
                                                            </button>
                                                            <label style="width: 45px;" class="btn btn-secondary btn-outline btn-primary" for="inputImage" title="Upload image file">
                                                                <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
                                                                <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs"> <span style="color: #ffffff;" class="fa fa-upload"></span> </span>
                                                            </label>
                                                            
                                                        </div>

                                                        <div class="btn-group btn-group-crop" style="margin-left: 5px;">
                                                            <!-- <div class="btn-group btn-group-crop"> -->
                                                                <button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 1170, &quot;height&quot;: 516 }" style="width: 45px;"> 
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                                <button type="button" class="btn btn-info" id="simpan"> 
                                                                    <span class="docs-tooltip">Simpan</span>
                                                                </button>
                                                            <!-- </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <hr>
                                                        
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="row el-element-overlay" id="list_img_send">
                                                                      
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.Croping of image -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © 2017 Admin Press Admin by themedesigner.in
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/popper.min.js"></script>

    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/custom.min.js"></script>
    <!-- Image cropper JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/cropper/cropper.min.js"></script>
    <!-- <script src="<?php print_r(base_url());?>assets/template/assets/plugins/cropper/cropper-init.js"></script> -->
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>

    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

<!-- <div></div> -->
    <script type="text/javascript">
        var obj_img = [];

        $(function () {
            'use strict';
            var console = window.console || { log: function () {} };
            var $image = $('#image');
            var $download = $('#download');
            var $dataX = $('#dataX');
            var $dataY = $('#dataY');
            var $dataHeight = $('#dataHeight');
            var $dataWidth = $('#dataWidth');
            var $dataRotate = $('#dataRotate');
            var $dataScaleX = $('#dataScaleX');
            var $dataScaleY = $('#dataScaleY');
            var options = {
                aspectRatio: 16 / 9,
                preview: '.img-preview',
                crop: function (e) {
                  $dataX.val(Math.round(e.x));
                  $dataY.val(Math.round(e.y));
                  $dataHeight.val(Math.round(e.height));
                  $dataWidth.val(Math.round(e.width));
                  $dataRotate.val(e.rotate);
                  $dataScaleX.val(e.scaleX);
                  $dataScaleY.val(e.scaleY);
                }
              };


            // Tooltip
            $('[data-toggle="tooltip"]').tooltip();


            // Cropper
            $image.on({
                'build.cropper': function (e) {
                console.log(e.type);
            },
                'built.cropper': function (e) {
                console.log(e.type);
            },
                'cropstart.cropper': function (e) {
                console.log(e.type, e.action);
            },
                'cropmove.cropper': function (e) {
                console.log(e.type, e.action);
            },
                'cropend.cropper': function (e) {
                console.log(e.type, e.action);
            },
                'crop.cropper': function (e) {
                console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
            },
                'zoom.cropper': function (e) {
                console.log(e.type, e.ratio);
            }
            }).cropper(options);


            // Buttons
            if (!$.isFunction(document.createElement('canvas').getContext)) {
                $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
            }

            if (typeof document.createElement('cropper').style.transition === 'undefined') {
                $('button[data-method="rotate"]').prop('disabled', true);
                $('button[data-method="scale"]').prop('disabled', true);
            }


            // Download
            // if (typeof $download[0].download === 'undefined') {
            //     $download.addClass('disabled');
            // }


            // Options
            $('.docs-toggles').on('change', 'input', function () {
                var $this = $(this);
                var name = $this.attr('name');
                var type = $this.prop('type');
                var cropBoxData;
                var canvasData;

                if (!$image.data('cropper')) {
                    return;
                }

                if (type === 'checkbox') {
                    options[name] = $this.prop('checked');
                    cropBoxData = $image.cropper('getCropBoxData');
                    canvasData = $image.cropper('getCanvasData');

                    options.built = function () {
                    $image.cropper('setCropBoxData', cropBoxData);
                    $image.cropper('setCanvasData', canvasData);
                    };
                } else if (type === 'radio') {
                options[name] = $this.val();
                }

                $image.cropper('destroy').cropper(options);
            });


            // Methods
            $('.docs-buttons').on('click', '[data-method]', function () {
                var $this = $(this);
                var data = $this.data();
                var $target;
                var result;

                if ($this.prop('disabled') || $this.hasClass('disabled')) {
                    return;
                }

                if ($image.data('cropper') && data.method) {
                    data = $.extend({}, data); // Clone a new one

                    if (typeof data.target !== 'undefined') {
                        $target = $(data.target);
                        if (typeof data.option === 'undefined') {
                            try {
                                data.option = JSON.parse($target.val());
                            } catch (e) {
                                console.log(e.message);
                            }
                        }
                    }

                    if (data.method === 'rotate') {
                        $image.cropper('clear');
                    }

                    result = $image.cropper(data.method, data.option, data.secondOption);
                    console.log($image.cropper(data.method, data.option, data.secondOption));

                    if (data.method === 'rotate') {
                        $image.cropper('crop');
                    }

                    switch (data.method) {
                        case 'scaleX':
                        case 'scaleY':
                          $(this).data('option', -data.option);
                          break;

                        case 'getCroppedCanvas':
                            if (result) {
                                
                                // console.log(result.toDataURL('image/jpeg'));
                                // Bootstrap's Modal
                                // $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                                obj_img.push(result.toDataURL('image/jpeg'));

                                // if (!$download.hasClass('disabled')) {
                                //   $download.attr('href', result.toDataURL('image/jpeg'));
                                // }

                                console.log(obj_img);
                                render_img_list();
                            }
                        break;
                    }

                    if ($.isPlainObject(result) && $target) {
                        try {
                            $target.val(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.message);
                        }
                    }

                }
            });

            function render_img_list(){
                var str_img = "";
                for(let item in obj_img){
                    // str_img += "<div style=\"padding: 10px\"><img src=\""+obj_img[item]+"\" style=\"display: block; width: 173.333px; height: 115.491px; min-width: 0px !important; min-height: 0px !important; max-width: none !important; max-height: none !important; transform: translateX(-17.3333px) translateY(-18.7453px);\"></div>";

                    str_img += "<div class=\"col-lg-3 col-md-6\">"+
                                    "<div class=\"card\" style=\"margin:0px;\">"+
                                        "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                            "<div class=\"el-card-avatar el-overlay-1\">"+
                                                "<img src=\""+obj_img[item]+"\">"+
                                                "<div class=\"el-overlay\">"+
                                                    "<ul class=\"el-info\">"+
                                                        "<li><a class=\"btn default btn-outline image-popup-vertical-fit\" href=\""+obj_img[item]+"\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                        "<li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\"><i class=\"icon-link\"></i></a></li>"+
                                                    "</ul>"+
                                                "</div>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>";
                }

                $("#list_img_send").html(str_img);
            }


            // Keyboard
            $(document.body).on('keydown', function (e) {

                if (!$image.data('cropper') || this.scrollTop > 300) {
                    return;
                }

                switch (e.which) {
                    case 37:
                        e.preventDefault();
                        $image.cropper('move', -1, 0);
                        break;

                    case 38:
                        e.preventDefault();
                        $image.cropper('move', 0, -1);
                        break;

                    case 39:
                        e.preventDefault();
                        $image.cropper('move', 1, 0);
                        break;

                    case 40:
                        e.preventDefault();
                        $image.cropper('move', 0, 1);
                        break;
                }
            });


            // Import image
            var $inputImage = $('#inputImage');
            var URL = window.URL || window.webkitURL;
            var blobURL;

            if (URL) {
                $inputImage.change(function () {
                    var files = this.files;
                    var file;

                    if (!$image.data('cropper')) {
                        return;
                    }

                    if (files && files.length) {
                    file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            blobURL = URL.createObjectURL(file);
                            $image.one('built.cropper', function () {

                            // Revoke when load complete
                            URL.revokeObjectURL(blobURL);
                            }).cropper('reset').cropper('replace', blobURL);
                            $inputImage.val('');
                        } else {
                            window.alert('Please choose an image file.');
                        }
                    }
                });
            } else {
                $inputImage.prop('disabled', true).parent().addClass('disabled');
            }
        });

        $("#simpan").click(function(){
            var data_main = new FormData();
            data_main.append('data_img', JSON.stringify(obj_img));

            $.ajax({
                url: "<?php echo base_url()."admin/postimagemain/save_image";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // response_login(res);
                    console.log(res);
                }
            });
        });
    </script>
    <div></div>
</body>

</html>
