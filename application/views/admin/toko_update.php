<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor">Edit Data Toko</h3>
    </div>

    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-info" id="btn_list_toko"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;List Toko</button>
        
        <button type="button" class="btn btn-rounded btn-danger" id="btn_del_toko"><i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;Hapus Toko</button>

        <button type="button" class="btn btn-rounded btn-success" id="btn_up_toko"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;Edit Toko</button>
        
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->

<?php
    if(isset($list_toko)){
        if($list_toko){

            $id_toko    = $list_toko["id_toko"];
            $tipe_owner = $list_toko["tipe_owner"];
            $id_owner   = $list_toko["id_owner"];
            $nama_toko  = $list_toko["nama_toko"];
            $desc_toko  = $list_toko["desc_toko"];
            $main_img_toko = $list_toko["main_img_toko"];
            $img_list_toko = $list_toko["img_list_toko"];


            $main_img_toko = str_replace("base_url/", base_url(), $main_img_toko);
            $img_list_toko = str_replace("base_url/", base_url(), $img_list_toko);
            $desc_toko = str_replace("base_url/", base_url(), $desc_toko);
            // print_r($list_toko);

            // $img_list_toko_js = [];
            // foreach ($img_list_toko as $key => $value) {
            //     $img_list_toko_js["img_".$key] = $value;
            // }

            // $img_list_toko_js = json_encode($img_list_toko_js);
        }
    }   
?>

<div class="container-fluid">
    <div class="row" id="row_img_list">
        <div class="col-lg-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Data Edit Toko</h4>
                </div>
                <div class="card-body collapse show">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Nama Toko</label>
                                        <input type="input" class="form-control" name="nama_toko" id="nama_toko" value="<?php print_r($nama_toko);?>">
                                        <p id="msg_nama_toko" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tipe Owner</label>
                                        <select class="select2 form-control custom-select" id="tipe_owner" name="tipe_owner" style="width: 100%; height:36px;">
                                            <option value="user">User</option>
                                            <option value="admin">Admin</option>
                                        </select>
                                        <p id="msg_tipe_owner" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Owner</label>
                                        <select class="select2 form-control custom-select" id="owner" name="owner" style="width: 100%; height:36px;">
                                            
                                        </select>
                                        <p id="msg_owner" style="color: red;"></p>
                                    </div>
                                </div>  
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Image Toko Utama</label>
                                        <input type="input" class="form-control" name="img_utama" id="img_utama" value="<?php print_r($main_img_toko);?>">
                                        <p id="msg_img_utama" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-3 text-left">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">&nbsp;</label><br>
                                        <button type="button" name="add_img_utama" id="add_img_utama" class="btn btn-info">add gambar utama</button>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-lg-3 col-md-6">
                                        <div class="card" style="margin-bottom: 10px; margin-top: 10px;">
                                            <div class="el-card-item" style="padding: 0px;">
                                                <div class="el-card-avatar el-overlay-1" style="margin: 0px;">
                                                    <img id="out_img_utama" name="out_img_utama" src="<?php print_r($main_img_toko);?>" width="290px" height="158px">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Deskripsi Toko</label>
                                <textarea class="summernote" name="article" id="article"><?php print_r($desc_toko);?></textarea>
                                <p id="msg_article" style="color: red;"></p>
                            </div>        
                        </div>

                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Image Toko</label>
                                <input type="input" class="form-control" name="img_list_toko" id="img_list_toko">
                                <p id="msg_img_list_toko" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-3 text-left">
                            <div class="form-group">
                                <label for="message-text" class="control-label">&nbsp;</label><br>
                                <button type="button" name="add_img_list" id="add_img_list" class="btn btn-info">add gambar</button>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="row">
                                <div class="row el-element-overlay" id="list_img_send">
                                </div>
                            </div>
                        </div>


                        <!-- <div class="col-md-12 text-right">
                                <button type="button" name="add_toko" id="add_toko" class="btn btn-info">Simpan</button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="row_img_list">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Image List</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div id="slimtest1">
                            <div class="row el-element-overlay" id="out_img_list"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="modal_img_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="d_title_img">Large modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-md-12">
                        <img src="" id="out_base_64" width="100%">  
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<?php
    if(isset($list_image)){
        if ($list_image) {
            $set_array = [];
            foreach ($list_image as $key => $value) {
                $id_img = $value->id_img;
                $title_img = $value->title_img;
                $path_img = $value->path_img;
                $file_img = $value->file_img;
                $category_img = json_decode($value->category_img);

                $set_array[$id_img] = ["title_img"=>$title_img,
                                        "path_img"=>$path_img,
                                        "file_img"=>$file_img,
                                        "category_img"=>$category_img,
                                    ];
            }

            $set_array = json_encode($set_array);
        }
    }
?>


<script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>

<script type="text/javascript">

    jQuery(document).ready(function() {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

        $("#row_img_list").hide(100);

    });

    $('#slimtest1').slimScroll({
        height: '400px'
    });

    var list_image = JSON.parse('<?php print_r($set_array);?>');
    var list_user = JSON.parse('<?php print_r(json_encode($list_user));?>');
    var list_img_toko = <?php print_r($img_list_toko);?>;
    
    var id_cache = "<?= $id_toko?>";
    var id_ch_cache = "";

    var all_var = "";

    $(document).ready(function(){
        render_img(list_image);
        check_tipe_user();

        render_img_list_toko();
    });

    //=========================================================================//
    //-----------------------------------set_list_img--------------------------//
    //=========================================================================//
        function render_img(data_json){
            console.log(data_json);
            var str_img = "";
            for (let item in data_json) {
                // console.log(data_json[item]);
                str_img += "<div class=\"col-lg-3 col-md-6\">"+
                                "<div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">"+
                                    "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                        "<div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\">"+
                                            "<img src=\"<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"\">"+
                                            "<div class=\"el-overlay\">"+
                                                "<ul class=\"el-info\">"+
                                                    "<li><a class=\"btn default btn-outline\" onclick=\"open_image('<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"')\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                    "<li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"copy_clip('<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"')\"><i class=\"icon-link\"></i></a></li>"+
                                                "</ul>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
            }

            $("#out_img_list").html(str_img);
            $("#row_img_list").show(200);
        }

        function copy_clip(text){
            copyTextToClipboard(text);
        }

        function open_image(id_item){
            var main_img     = "<?php print_r(base_url()); ?>"+list_image[id_item].path_img+list_image[id_item].file_img;
            var title_img    = list_image[id_item].title_img;
            var category_img = list_image[id_item].category_img.join(", ");

            $("#d_title_img").html(title_img);
            $("#d_category_img").html(category_img);
            
            $("#out_base_64").attr("src", main_img);

            $("#modal_img_detail").modal("show");
        }
    //=========================================================================//
    //-----------------------------------set_list_img--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------set_user------------------------------//
    //=========================================================================//
        $("#tipe_owner").change(function(){
            check_tipe_user();
        });

        function check_tipe_user(){
            var tipe_owner = $("#tipe_owner").val();

            if(tipe_owner == "admin"){
                render_admin();
            }else{
                render_user(list_user);
            }
        }

        function render_user(data_json){
            console.log(data_json);
            var str_img = "";
            for (let item in data_json) {
                // console.log(data_json[item]);
                str_img += "<option value=\""+data_json[item].username_user+"\">"+data_json[item].username_user+"</option>";
            }

            $("#owner").html(str_img);
        }

        function render_admin(){
            var str_img = "<option value=\"admin\">admin</option>";
            
            $("#owner").html(str_img);
        }

        function copy_clip(text){
            copyTextToClipboard(text);
        }

        function open_image(id_item){
            var main_img     = "<?php print_r(base_url()); ?>"+list_image[id_item].path_img+list_image[id_item].file_img;
            var title_img    = list_image[id_item].title_img;
            var category_img = list_image[id_item].category_img.join(", ");

            $("#d_title_img").html(title_img);
            $("#d_category_img").html(category_img);
            
            $("#out_base_64").attr("src", main_img);

            $("#modal_img_detail").modal("show");
        }
    //=========================================================================//
    //-----------------------------------set_user------------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------create_list_img-----------------------//
    //=========================================================================//
        $("#add_img_list").click(function(){
            var val_img_toko = $("#img_list_toko").val();

            // var arr_ln = Object.keys(list_img_toko).length;
            var arr_ln = list_img_toko.length;


            console.log(arr_ln);

            if(arr_ln <= 4){
                if(val_img_toko != ""){
                    list_img_toko.push(val_img_toko);
                    // list_img_toko["img_"+(arr_ln+1)] = val_img_toko;
                    create_alert("Proses Berhasil", "Gambar berhasil ditambahkan", "success");

                    render_img_list_toko();
                }else{
                    create_alert("Proses Gagal", "Gambar sudah berada dilist", "error");
                }
            }else{
                create_alert("Proses Gagal", "Max list gambar adalah 5 ", "error");
            }

            console.log(list_img_toko);
        });

        function render_img_list_toko(){
            var str_img = "";
            for(let item in list_img_toko){

                var src_img = list_img_toko[item];
            
                str_img += "<div class=\"col-lg-3 col-md-6\">"+
                                "<div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">"+
                                    "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                        "<div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\">"+
                                            "<img src=\""+src_img+"\">"+
                                            "<div class=\"el-overlay\">"+
                                                "<ul class=\"el-info\">"+
                                                    "<li><a class=\"btn default btn-outline\" onclick=\"open_image('"+src_img+"')\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                    "<li><a class=\"btn default btn-outline\" onclick=\"del_image('"+item+"')\"><i class=\"icon-close\"></i></a></li>"+
                                                "</ul>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
            }

            $("#list_img_send").html(str_img);
        }


        function open_image(link_img){
            $("#d_title_img").html("Image Detail");
            $("#out_base_64").attr("src", link_img);
            $("#modal_img_detail").modal("show");
        }

        function del_image(id_item){
            // delete list_img_toko[id_item];
            list_img_toko.splice(id_item,1);
            create_alert("Success", "Foto berhasil dihapus dari list", "error");
            render_img_list_toko();
        }
    //=========================================================================//
    //-----------------------------------create_list_img-----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------create_img_utama----------------------//
    //=========================================================================//
        $("#add_img_utama").click(function(){
            var img_utama = $("#img_utama").val();

            $("#out_img_utama").attr("src", img_utama);
        });
    //=========================================================================//
    //-----------------------------------create_img_utama----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------simpan_toko---------------------------//
    //=========================================================================//
        $("#btn_up_toko").click(function(){
            var img_utama = $("#out_img_utama").attr('src');
            // console.log();
            if(img_utama != ""){
                if (typeof list_img_toko !== 'undefined' && list_img_toko.length > 0) {
                    simpan_toko();
                }
            }
        });

        function simpan_toko(){
            var data_main = new FormData();
            data_main.append('id_toko'      , id_cache);

            data_main.append('tipe_owner'   , $("#tipe_owner").val());
            data_main.append('owner'        , $("#owner").val());
            data_main.append('nama_toko'    , $("#nama_toko").val());
            data_main.append('desc_toko'    , $("#article").val());
            data_main.append('main_img_toko', $("#out_img_utama").attr('src'));

            data_main.append('img_list_toko', JSON.stringify(list_img_toko));
            
            $.ajax({
                url: "<?php echo base_url()."admin/tokomain/update_toko";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        }

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                clear_form_insert();
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/tokomain/index_update/");?>"+id_cache);
            } else {
                $("#msg_tipe_owner").html(detail_msg.tipe_owner);
                $("#msg_owner").html(detail_msg.owner);
                $("#msg_nama_toko").html(detail_msg.nama_toko);
                $("#msg_article").html(detail_msg.desc_toko);
                $("#msg_img_utama").html(detail_msg.main_img_toko);
                $("#msg_img_list_toko").html(detail_msg.img_list_toko);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#tipe_owner").val("admin");
            $("#owner").val("admin");

            $("#nama_toko").val("");
            $("#desc_toko").val("");

            $("#img_utama").attr('src', "")
            
        }
    //=========================================================================//
    //-----------------------------------simpan_toko---------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------delete_toko---------------------------//
    //=========================================================================//

        $("#btn_del_toko").click(function(){
            delete_toko(id_cache);
        });

        function delete_toko(id_toko){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_toko);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function method_delete(id_toko){
            var data_main = new FormData();
            data_main.append('id_toko', id_toko);

            $.ajax({
                url: "<?php echo base_url()."admin/tokomain/delete_toko";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/toko_list");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------delete_toko---------------------------//
    //=========================================================================// 

        $("#btn_list_toko").click(function(){
            window.location.href = "<?= base_url();?>admin/toko_list";
        });
    
</script>

