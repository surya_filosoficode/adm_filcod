<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor">Data Product</h3>
    </div>
    
    <div class="col-md-6 text-right">
        <a class="btn btn-info" id="btn_add_product" href="<?php print_r(base_url());?>admin/choose_toko">Tambah Product</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <!-- Card -->
                <div class="card">
                    <div class="card-body">
                        <div class="row el-element-overlay">
                            <?php
                                if(isset($list_product)){
                                    if($list_product){
                                        foreach ($list_product as $key => $value) {
                                            // print_r($value);
                                            $id_product     = $value->id_product;
                                            $nama_product   = $value->nama_product;

                                            $id_brand       = $value->id_brand;
                                            // $nama_brand     = $value->nama_brand;

                                            $id_toko        = $value->id_toko;
                                            $nama_toko      = $value->nama_toko;

                                            $category_produk    = $value->category_produk;
                                            $desc_product       = $value->desc_product;
                                            $spec_product       = $value->spec_product;
                                            $img_list_product   = $value->img_list_product;
                                            $tag_product        = $value->tag_product;
                                            $price_product      = $value->price_product;

                                            $img_list_product = str_replace("base_url/", base_url(), $img_list_product);
                                            $desc_product = str_replace("base_url/", base_url(), $desc_product);

                                            $page_img = "";
                                            $one_page = json_decode($img_list_product);
                                            if($one_page){
                                                $page_img = $one_page[0];
                                            }
                            ?>
                                <div class="col-lg-3 col-md-6">
                                    <div class="card">
                                        <div class="el-card-item" style="padding: 0px;">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?=$page_img?>" alt="user" />
                                                <div class="el-overlay scrl-up">
                                                    <ul class="el-info">
                                                        <li>
                                                            <a class="btn default btn-outline" onclick="detail_product('<?=$id_product?>')" href="javascript:void(0);">
                                                                <i class="icon-list"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="btn default btn-outline" href="javascript:void(0);" onclick="delete_product('<?=$id_product?>')">
                                                                <i class="icon-trash"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="el-card-content">
                                                <b><h3 class="box-title"><?=$nama_product?></h3></b>
                                                <h6>Toko. <?=$nama_toko?></h6>
                                                <br/> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                        }
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- Card -->
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>

<script type="text/javascript">
    function detail_product(id_product){
        window.location.href = "<?php print_r(base_url());?>admin/productmain/index_update/"+id_product;
    }

    //=========================================================================//
    //-----------------------------------delete_product---------------------------//
    //=========================================================================//
        function delete_product(id_product){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_product);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function method_delete(id_product){
            var data_main = new FormData();
            data_main.append('id_product', id_product);

            $.ajax({
                url: "<?php echo base_url()."admin/productmain/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/product_list");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------delete_product---------------------------//
    //=========================================================================// 
</script>