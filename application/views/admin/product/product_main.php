<?php
    // print_r($list_toko);
    $id_toko = "";
    $nama_toko = "";
    if(isset($list_toko)){
        if($list_toko){
            $id_toko    = $list_toko["id_toko"];
            $nama_toko  = $list_toko["nama_toko"];
        }
    }
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor">Create Product</h3>
    </div>

    <div class="col-md-6 text-right">
        <a class="btn btn-rounded btn-info" href="<?=base_url()."admin/product_list_af_toko/".$id_toko; ?>"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;List Product</a>
        
        <!-- <button type="button" class="btn btn-rounded btn-danger" id="btn_del_article"><i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;Hapus Product</button> -->

        <button type="button" class="btn btn-rounded btn-success" id="btn_add_product"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;Simpan Product</button>
        
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h5>Pilih Image</h5></div>
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    </div> -->
    <div class="row" id="row_img_list">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Image List</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div id="slimtest1">
                            <div class="row el-element-overlay" id="out_img_list"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>           

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Add Product</h5>
                        </div>
                        <!-- <div class="col-md-6 text-right">
                            <a href="#" onclick="get_image()" style="font-size: 15px;"><i class="mdi mdi-folder-image"></i> &nbsp;&nbsp; Lihat Gambar</a>        
                        </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Nama Product</label>
                                        <input type="text" class="form-control" name="nama_product" id="nama_product">
                                        <p id="msg_nama_product" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Toko</label>
                                        <input type="text" class="form-control" name="nama_toko" id="nama_toko" value="<?=$nama_toko?>" readonly="">
                                        <p id="msg_nama_toko" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Brand</label><br>
                                        <select class="select2 form-control custom-select" name="brand" id="brand" style="width: 100%; height:36px;">
                                            <?php 
                                                if(isset($list_brand)){
                                                    if($list_brand){
                                                        foreach ($list_brand as $key => $value) {
                                                            print_r("<option value=\"".$value->nama_brand."\">".$value->nama_brand."</option>");
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                        
                                        <p id="msg_brand" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Category Produk</label>
                                        
                                        <select class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" name="category_produk" id="category_produk">
                                            <option value="default">Default</option>
                                            <?php
                                                if(isset($list_jenis)){
                                                    if($list_jenis){
                                                        foreach ($list_jenis as $key => $value) {
                                                            print_r("<option value=\"".$value->nama_jenis."\">".$value->nama_jenis."</option>");
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                        <p id="msg_category_produk" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">TAG Produk</label><br>
                                        <select multiple data-role="tagsinput" name="tag_produk" id="tag_produk">
                                        </select>
                                        <!-- <input type="input" class="form-control" name="tag_produk" id="tag_produk"> -->
                                        <p id="msg_tag_produk" style="color: red;"></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Status Harga Produk</label>
                                        
                                        <select class="form-control" name="sts_pr_prd" id="sts_pr_prd">
                                            <option value="0">Harga Ditentukan</option>
                                            <option value="1">Harga Negosiasi</option>
                                        </select>
                                        <p id="msg_sts_pr_prd" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Harga Product</label>&nbsp;(<label id="val_harga_produk">Rp. 0</label>)
                                        <input type="number" class="form-control" name="harga_produk" id="harga_produk">
                                        <p id="msg_harga_produk" style="color: red;"></p>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Discount Harga</label>&nbsp;(<label id="val_disc_produk">Rp. 0</label>)
                                        <input type="number" class="form-control" name="disc_produk" id="disc_produk" value="0">
                                        <p id="msg_disc_produk" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Status Negosiasi</label>
                                        
                                        <select class="form-control" name="sts_nego" id="sts_nego">
                                            <option value="0">tidak bisa dinego</option>
                                            <option value="1">bisa dinego</option>
                                        </select>
                                        <p id="msg_sts_nego" style="color: red;"></p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Article</label>
                                <textarea class="summernote" name="article" id="article"></textarea>
                                <p id="msg_article" style="color: red;"></p>
                            </div>        
                        </div>
                        
                         <div class="col-md-7">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Image Product</label>
                                <input type="text" class="form-control" name="img_list_product" id="img_list_product">
                                <p id="msg_img_list_product" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-3 text-left">
                            <div class="form-group">
                                <label for="message-text" class="control-label">&nbsp;</label><br>
                                <button type="button" name="add_img_list" id="add_img_list" class="btn btn-info">add gambar</button>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="row">
                                <div class="row el-element-overlay" id="list_img_send">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->         
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->


    <div class="modal fade bs-example-modal-lg" id="modal_img_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-nama_product" id="d_nama_product_img">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label"><b>Categori/Brand</b></label>
                                <h6 id="d_nama_tokogory_img"></h6>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <img src="" id="out_base_64" width="100%">  
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<?php
    if(isset($list_image)){
        if ($list_image) {
            $set_array = [];
            foreach ($list_image as $key => $value) {
                $id_img = $value->id_img;
                $title_img = $value->title_img;
                $path_img = $value->path_img;
                $file_img = $value->file_img;
                $category_img = json_decode($value->category_img);

                $set_array[$id_img] = ["title_img"=>$title_img,
                                        "path_img"=>$path_img,
                                        "file_img"=>$file_img,
                                        "category_img"=>$category_img,
                                    ];
            }

            $set_array = json_encode($set_array);
        }
    }
?>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
    <script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>
    <script>
    var all_var = "";
    var list_img_product = [];
    var list_image = JSON.parse('<?php print_r($set_array);?>');

    jQuery(document).ready(function() {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

        $("#row_img_list").hide(100);

    });

    $('#slimtest1').slimScroll({
        height: '400px'
    });

    $(document).ready(function(){
        sts_pr_prd_ch();

        render_img(list_image);
    });

    function currency(x){
        return x.toLocaleString('us-EG');
    }

    //=========================================================================//
    //-----------------------------------sts_pr_prd_ch-------------------------//
    //=========================================================================//
        $("#sts_pr_prd").change(function(){
            sts_pr_prd_ch();
        });

        function sts_pr_prd_ch(){
            var sts_pr_prd = $("#sts_pr_prd").val();
            ch_harga_produk();
            ch_disc_produk();
            $("#sts_nego").val(1);
            if(sts_pr_prd == "0"){
                $("#harga_produk").removeAttr("readonly", true);
                $("#disc_produk").removeAttr("readonly", true);
                $("#sts_nego").removeAttr("readonly", true);            
            }else{
                $("#harga_produk").val("0");
                $("#harga_produk").attr("readonly", true);
                $("#disc_produk").attr("readonly", true);
                $("#sts_nego").attr("readonly", true);     
            }
        }
    //=========================================================================//
    //-----------------------------------sts_pr_prd_ch-------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------create_list_img-----------------------//
    //=========================================================================//
        $("#add_img_list").click(function(){
            var val_img_toko = $("#img_list_product").val();

            var arr_ln = list_img_product.length;

            console.log(arr_ln);

            if(list_img_product.length <= 7){
                if(list_img_product.indexOf(val_img_toko) && val_img_toko != ""){
                    list_img_product.push(val_img_toko);
                    create_alert("Proses Berhasil", "Gambar berhasil ditambahkan", "success");

                    render_img_list_product();
                }else{
                    create_alert("Proses Gagal", "Gambar sudah berada dilist", "error");
                }
            }else{
                create_alert("Proses Gagal", "Max list gambar adalah 5 ", "error");
            }

            console.log(list_img_product);
        });

        function render_img_list_product(){
            var str_img = "";
            for(let item in list_img_product){

                var src_img = list_img_product[item];
            
                str_img += "<div class=\"col-lg-3 col-md-6\">"+
                                "<div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">"+
                                    "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                        "<div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\">"+
                                            "<img src=\""+src_img+"\">"+
                                            "<div class=\"el-overlay\">"+
                                                "<ul class=\"el-info\">"+
                                                    "<li><a class=\"btn default btn-outline\" onclick=\"open_image_add('"+src_img+"')\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                    "<li><a class=\"btn default btn-outline\" onclick=\"del_image('"+item+"')\"><i class=\"icon-close\"></i></a></li>"+
                                                "</ul>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
            }

            $("#list_img_send").html(str_img);
        }


        function open_image_add(link_img){
            $("#d_title_img").html("Image Detail");
            $("#out_base_64").attr("src", link_img);
            $("#modal_img_detail").modal("show");
        }

        function del_image(id_item){
            delete list_img_product[id_item];
            create_alert("Success", "Foto berhasil dihapus dari list", "error");
            render_img_list_product();
        }
    //=========================================================================//
    //-----------------------------------create_list_img-----------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------image_action--------------------------//
    //=========================================================================//
        function render_img(data_json){
            // console.log(data_json);
            var str_img = "";
            for (let item in data_json) {
                // console.log(data_json[item]);
                var title_img_show = data_json[item].category_img.join(", ");

                if(title_img_show >= 18){
                    var title_img_show = title_img_show.substr(0, 18) + " ...";
                }

                str_img += "<div class=\"col-lg-3 col-md-6\">"+
                                "<div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">"+
                                    "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                        "<div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\">"+
                                            "<img src=\"<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"\">"+
                                            "<div class=\"el-overlay\">"+
                                                "<ul class=\"el-info\">"+
                                                    "<li><a class=\"btn default btn-outline\" onclick=\"open_image('"+item+"')\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                    "<li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"copy_clip('<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"')\"><i class=\"icon-link\"></i></a></li>"+
                                                "</ul>"+
                                            "</div>"+
                                            "<div style=\"padding: 2px;\">"+
                                                "<label>"+title_img_show+"</label>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
            }

            $("#out_img_list").html(str_img);
            $("#row_img_list").show(200);
        }

        function copy_clip(text){
            copyTextToClipboard(text);
        }

        function sip(){
            console.log(all_var);
        }

        function open_image(id_item){
            var main_img     = "<?php print_r(base_url()); ?>"+list_image[id_item].path_img+list_image[id_item].file_img;
            var title_img    = list_image[id_item].title_img;
            var category_img = list_image[id_item].category_img.join(", ");

            $("#d_title_img").html(title_img);
            $("#d_category_img").html(category_img);
            
            $("#out_base_64").attr("src", main_img);

            $("#modal_img_detail").modal("show");
        }
    //=========================================================================//
    //-----------------------------------image_action--------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------harga_produk--------------------------//
    //=========================================================================//
        $("#harga_produk").keyup(function(){
            ch_harga_produk();
        });

        function ch_harga_produk(){
            var harga_produk = $("#harga_produk").val();

            $("#val_harga_produk").html("Rp. "+currency(parseFloat(harga_produk)));
        }
    //=========================================================================//
    //-----------------------------------harga_produk--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------disc_produk---------------------------//
    //=========================================================================//
        $("#disc_produk").keyup(function(){
            ch_disc_produk();
        });

        function ch_disc_produk(){
            var disc_produk = $("#disc_produk").val();

            $("#val_disc_produk").html("Rp. "+currency(parseFloat(disc_produk)));
        }
    //=========================================================================//
    //-----------------------------------harga_produk--------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------add_post------------------------------//
    //=========================================================================//
        function clear_form_add(){
            $("#sts_pr_prd").val("0");
            
            $("#harga_produk").val("0");
            $("#disc_produk").val("0");
            $("#sts_nego").val("0");  

            $("#nama_product").val("");
            $("#nama_toko").val("");

            // $("#harga_produk").val("");
            $("#val_harga_produk").html("Rp. 0");
            
            $("#brand").val(null).trigger("change");
            
            $("#category_produk").val(null).trigger("change");
            $('#tag_produk').tagsinput('removeAll');

            $("#article").html("");

            $("#img_list_product").val("");
            $("#list_img_send").html("");
        }

        $("#btn_add_product").click(function(){
            add_product();
        });


        function add_product(){
            var data_main = new FormData();
            data_main.append('nama_product'    , $("#nama_product").val());
            data_main.append('toko'    , "<?= $id_toko; ?>");
            data_main.append('brand'    , $("#brand").val());

            data_main.append('harga_produk'    , $("#harga_produk").val());
            data_main.append('sts_pr_prd'    , $("#sts_pr_prd").val());
            data_main.append('disc_produk'    , $("#disc_produk").val());
            data_main.append('sts_nego'    , $("#sts_nego").val());

            data_main.append('category_produk'    , JSON.stringify($("#category_produk").val()));
            data_main.append('tag_produk'    , JSON.stringify($("#tag_produk").tagsinput('items')));
            data_main.append('article'    , $("#article").val());
            data_main.append('img_list_product'    , JSON.stringify(list_img_product));
            
            // console.log();
            
            $.ajax({
                url: "<?php echo base_url()."admin/productmain/save";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    // console.log(res);
                }
            });
        }

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                clear_form_add();
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/product_list_af_toko/".$id_toko);?>");
            } else {
                $("#msg_nama_product").html(detail_msg.nama_product);
                $("#msg_nama_toko").html(detail_msg.toko);
                $("#msg_brand").html(detail_msg.brand);
                $("#msg_harga_produk").html(detail_msg.harga_produk);
                $("#msg_disc_produk").html(detail_msg.disc_produk);
                $("#msg_category_produk").html(detail_msg.category_produk);
                $("#msg_tag_produk").html(detail_msg.tag_produk);
                $("#msg_article").html(detail_msg.article);
                
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------add_post------------------------------//
    //=========================================================================//

    

    </script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
