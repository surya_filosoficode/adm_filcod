            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Master List Image</h3>
                </div>
                <div class="col-md-5 text-right">
                     <select class="select2 m-b-10 select2-multiple" id="category_img" name="category_img" style="width: 100%" multiple="multiple" data-placeholder="Choose">
                        <optgroup label="Alaskan/Hawaiian Time Zone">
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                        </optgroup>
                        <optgroup label="Pacific Time Zone">
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR">Oregon</option>
                            <option value="WA">Washington</option>
                        </optgroup>
                        
                    </select>
                </div>
                <div class="col-md-2 text-right">
                    <a class="btn btn-info" id="form_add_image" href="<?php print_r(base_url());?>admin/data_image">Tambah Gambar</a>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="row el-element-overlay">

                                        <?php 
                                        if(isset($list_image)){
                                            foreach ($list_image as $key => $value) {
                                                // print_r($value->dir);
                                                $category_img = json_decode($value->category_img);
                                                $category_str = implode(", ", $category_img);

                                                if(strlen($category_str) < 30){
                                                    $title_img_show = substr($category_str, 0, 30)." ...";
                                                }

                                                print_r("<div class=\"col-lg-3 col-md-6\">
                                                            <div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">
                                                                <div class=\"el-card-item\" style=\"padding: 0px;\">
                                                                    <div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\"> <img src=\"".base_url().$value->path_img.$value->file_img."\" alt=\"user\">
                                                                        <div class=\"el-overlay\">
                                                                            <ul class=\"el-info\">
                                                                                <li><a class=\"btn default btn-outline image-popup-vertical-fit\" href=\"".base_url().$value->path_img.$value->file_img."\"><i class=\"icon-magnifier\"></i></a></li>
                                                                                <li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"copy_clip('".base_url().$value->path_img.$value->file_img."')\"><i class=\"icon-link\"></i></a></li>
                                                                                <li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"del_main('".$value->id_img."')\"><i class=\"icon-trash\"></i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div style=\"padding: 2px;\">
                                                                        <b><label>".$category_str."</label></b>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>");
                                            }
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

    });

    function alert_confirm(title, msg, status, txt_cencel_btn, id){
        ! function($) {
            "use strict";
            var SweetAlert = function() {};
            SweetAlert.prototype.init = function() {
                swal({
                    title: title,
                    text: msg,
                    type: status,
                    showCancelButton: true,
                    confirmButtonColor: "#ffb22b",
                    confirmButtonText: txt_cencel_btn,
                    closeOnConfirm: false
                }, function() {
                    del_img_func(id);
                    swal.close();
                });
            },
            //init
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
    }

    function copy_clip(text){
        copyTextToClipboard(text);
    }

    function del_main(id){
        alert_confirm("Pesan Konfirmasi.!!", "Apa anda yakin ingin menghapus Gambar ini ?", "warning", "Hapus", id);
    }

    function del_img_func(id){
        var data_main = new FormData();
        data_main.append('param', id);

        // console.log(id);
        $.ajax({
            url: "<?php echo base_url()."admin/imagemain/delete_image";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                response_del_fucn(res);
            }
        });
    }

    function response_del_fucn(res){
        console.log(res);
        var data_json = JSON.parse(res);
        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        // console.log(data_json);
        if (main_msg.status) {
            window.location.href = "<?php print_r(base_url());?>admin/data_image_list";
            // create_sweet_alert("Pesan Success!!", "Gambar ini Berhasil dihapus", "success", "");
        } else {
            create_sweet_alert("Pesan Gagal!!", "Gambar ini Gagal dihapus", "warning", "");
        }
        
    }
</script>