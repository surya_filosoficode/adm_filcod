<?php
    $ar_month = $this->time_master->set_indo_month();
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor">Data Article</h3>
    </div>
    
    <div class="col-md-6 text-right">
        <a class="btn btn-info" id="btn_add_article" href="<?php print_r(base_url());?>admin/data_post_article">Tambah Article</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Jenis Article</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No. </th>
                                        <th width="15">Title Article</th>
                                        <th width="20%">Category Article</th>
                                        <th width="20%">Tag Article</th>
                                        <th width="15%">Check Article</th>
                                        <th width="15%">Post Article</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="main_table_content">
                                    <?php
                                        if(!empty($list_article)){
                                            $no = 1; 
                                            foreach ($list_article as $key => $value) {
                                                $id_article_main = $value->id_article_main;
                                                $title_article = $value->title_article;

                                                $category_article = json_decode($value->category_article);
                                                $tag_article = json_decode($value->tag_article);

                                                $main_img_article = $value->main_img_article;
                                                $content_article = $value->content_article;

                                                $create_admin_article = $value->create_admin_article;
                                                $create_date_article = $value->create_date_article;

                                                $status_check_article = $value->status_check_article;
                                                $admin_check_article = $value->admin_check_article;
                                                $date_check_article = $value->date_check_article;

                                                $str_status_check = "Belum Diperiksa";
                                                if($status_check_article == "1"){
                                                    $str_status_check = "Sudah Diperiksa";

                                                    // $arr_date_check = explode("-", $date_check_article);
                                                    //     $m_check = 
                                                }

                                                $status_post_article = $value->status_post_article;
                                                $admin_post_article = $value->admin_post_article;
                                                $date_post_article = $value->date_post_article;

                                                $str_status_post = "Pending";
                                                if($status_post_article == "1"){
                                                    $str_status_post = "Posting";
                                                }

                                                print_r("<tr>
                                                        <td>".$no."</td>
                                                        <td>".$title_article."</td>
                                                        <td>".implode(", ", $category_article)."</td>
                                                        <td>".implode(", ", $tag_article)."</td>

                                                        <td>".$str_status_check."</td>
                                                        <td>".$str_status_post."</td>

                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_article\" onclick=\"detail_promote('".$value->id_article_main ."')\" style=\"width: 40px;\"><i class=\"fa fa-list-alt\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-primary\" id=\"del_article\" onclick=\"add_promote('".$value->id_article_main ."')\" style=\"width: 40px;\"><i class=\"fa fa-plus-square\"></i></button>
                                                            </center>
                                                        </td>
                                                    </tr>");

                                                $no++;
                                            }
                                        }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Promote Article</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive m-t-40">
                            <table id="myTable1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No. </th>
                                        <th width="15">Title Article</th>
                                        <th width="20%">Category Article</th>
                                        <th width="20%">Tag Article</th>
                                        <th width="15%">Status Promote</th>
                                        <!-- <th width="15%">Post Article</th> -->
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="main_table_content">
                                    <?php
                                        if(!empty($list_pr_article)){
                                            $no = 1; 
                                            foreach ($list_pr_article as $key => $value) {
                                                $id_article_main = $value->id_article_main;
                                                $title_article = $value->title_article;

                                                $category_article = json_decode($value->category_article);
                                                $tag_article = json_decode($value->tag_article);

                                                $main_img_article = $value->main_img_article;
                                                $content_article = $value->content_article;

                                                $create_admin_article = $value->create_admin_article;
                                                $create_date_article = $value->create_date_article;

                                                $status_check_article = $value->status_check_article;
                                                $admin_check_article = $value->admin_check_article;
                                                $date_check_article = $value->date_check_article;

                                                $str_status_check = "Belum Diperiksa";
                                                if($status_check_article == "1"){
                                                    $str_status_check = "Sudah Diperiksa";

                                                    // $arr_date_check = explode("-", $date_check_article);
                                                    //     $m_check = 
                                                }

                                                $status_post_article = $value->status_post_article;
                                                $admin_post_article = $value->admin_post_article;
                                                $date_post_article = $value->date_post_article;

                                                $str_status_post = "Pending";
                                                if($status_post_article == "1"){
                                                    $str_status_post = "Posting";
                                                }


                                                $str_active_pr = "Not Active";
                                                if($value->active_pr == "1"){
                                                    $str_active_pr = "Active";
                                                }

                                                print_r("<tr>
                                                        <td>".$no."</td>
                                                        <td>".$title_article."</td>
                                                        <td>".implode(", ", $category_article)."</td>
                                                        <td>".implode(", ", $tag_article)."</td>

                                                        <td>".$str_active_pr."</td>

                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_article\" onclick=\"detail_promote('".$value->id_pr ."')\" style=\"width: 40px;\"><i class=\"fa fa-list-alt\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_article\" onclick=\"delete_promote('".$value->id_pr ."')\" style=\"width: 40px;\"><i class=\"fa fa-trash\"></i></button>
                                                            </center>
                                                        </td>
                                                    </tr>");

                                                $no++;
                                            }
                                        }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>

<script type="text/javascript">
    function detail_promote(id_article){
        window.open("<?php print_r(base_url());?>admin/articlemain/index_update/"+id_article);
    }

    //=========================================================================//
    //-----------------------------------add_promote---------------------------//
    //=========================================================================//
        function add_promote(id_article){
            var data_main = new FormData();
            data_main.append('id_article_pr', id_article);

            $.ajax({
                url: "<?php echo base_url()."admin/promotemain/add_promote";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_add_promote(res);
                }
            });
        }

        function response_add_promote(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/promote_article");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------add_promote---------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------delete_promote---------------------------//
    //=========================================================================//
        function delete_promote(id_pr){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_pr);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function method_delete(id_pr){
            var data_main = new FormData();
            data_main.append('id_pr', id_pr);

            $.ajax({
                url: "<?php echo base_url()."admin/promotemain/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/promote_article");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------delete_promote---------------------------//
    //=========================================================================// 
</script>