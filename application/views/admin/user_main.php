<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data User</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data User</h4>

                </div>
                <div class="card-body">
                    <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_user"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data User</button>

                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="12%">Username</th>
                                    <th width="13%">Email</th>
                                    <th width="10%">Tlp.</th>
                                    <th width="15%">Nama</th>
                                    <th width="10%">Tipe</th>
                                    <th width="10%">Password</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="main_table_content">
                                <?php
                                    if(!empty($list_data)){
                                        foreach ($list_data as $r_user => $v_user) {
                                            $str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                                                if($v_user->status_active_user == "1"){
                                                    $str_active = "<span class=\"label label-info\">aktif</span>";
                                                }elseif ($v_user->status_active_user == "2") {
                                                    $str_active = "<span class=\"label label-danger\">di blockir</span>";
                                                }

                                            $str_lv = "User Contributor";
                                                if($v_user->id_tipe_user == "1"){
                                                    $str_lv = "User Pelanggan";
                                                }

                                            $str_btn_active = "<button class=\"btn btn-primary\" id=\"un_ac_user\" onclick=\"disabled_user('".$v_user->id_user."')\" style=\"width: 30px; height: 30px; padding: 0px 0px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                                                
                                                if($v_user->status_active_user != "1"){
                                                    $str_btn_active = "<button class=\"btn btn-success\" id=\"ac_user\" onclick=\"active_user('".$v_user->id_user."')\" style=\"width: 30px; height: 30px; padding: 0px 0px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                                                }

                                            $str_btn_ch_pass = "<a id=\"btn_ch_pass\" href=\"#\" onclick=\"ch_pass('".$v_user->id_user."')\">Ubah Password</a>";

                                            echo "<tr>
                                                <td>".($r_user+1)."</td>
                                                <td>".$v_user->username_user."</td>
                                                <td>".$v_user->email_user."</td>
                                                <td>".$v_user->tlp_user."</td>
                                                <td>".$v_user->nama_user."</td>
                                                <td>".$str_lv."</td>
                                                <td>".$str_btn_ch_pass."</td>
                                                <td>".$str_active."</td>
                                                
                                                
                                                <td>
                                                    <center>
                                                    ".$str_btn_active."
                                                    <button class=\"btn btn-info\" id=\"up_user\" onclick=\"update_user('".$v_user->id_user."')\" style=\"width: 30px; height: 30px; padding: 0px 0px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                    <button class=\"btn btn-danger\" id=\"del_user\" onclick=\"delete_user('".$v_user->id_user."')\" style=\"width: 30px; height: 30px; padding: 0px 0px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                    </center>
                                                </td>
                                                </tr>";
                                        }
                                    }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="change_pass_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Ubah Password User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="ch_pass" name="pass" required="">
                                <p id="_msg_ch_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="ch_repass" name="repass" required="">
                                <p id="_msg_ch_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="add_pass_new" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_user------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_user" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."user_super/superuser/insert_user";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Add User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tipe<span style="color: red;">*</span></label>
                                <select class="form-control" id="id_tipe_user" name="id_tipe_user">
                                    <option value="0">User</option>
                                    <!-- <option value="1">Frontliner</option> -->
                                </select>
                                <p id="msg_id_tipe_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="email_user" name="email_user" required="">
                                <p id="msg_email_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_user" name="nama_user" required="">
                                <p id="msg_nama_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">No. Handphone <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="tlp_user" name="tlp_user" required="">
                                <p id="msg_tlp_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="alamat_user" name="alamat_user" required="">
                                <p id="msg_alamat_user" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Username <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="username" name="username" required="">
                                <p id="msg_username" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="pass" name="pass" required="">
                                <p id="msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="repass" name="repass" required="">
                                <p id="msg_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_user" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_user------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_user------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_user" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."user_super/superuser/update_user";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tipe <span style="color: red;">*</span></label>
                                <select class="form-control" id="_id_tipe_user" name="id_tipe_user">
                                    <option value="0">user</option>
                                    <option value="1">Frontliner</option>
                                </select>
                                <p id="_msg_id_tipe_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_email_user" name="email_user" required="">
                                <p id="_msg_email_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nama_user" name="nama_user" required="">
                                <p id="_msg_nama_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">No. Handphone <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="_tlp_user" name="tlp_user" required="">
                                <p id="_msg_tlp_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_alamat_user" name="alamat_user" required="">
                                <p id="_msg_alamat_user" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Username <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_username" name="username" required="">
                                <p id="_msg_username" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_user" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_user------------------------ -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------insert_user--------------------------//
    //=========================================================================//
        $("#add_user").click(function() {
            var data_main = new FormData();
            data_main.append('id_tipe_user' , $("#id_tipe_user").val());
            data_main.append('email_user'   , $("#email_user").val());
            data_main.append('username'     , $("#username").val());
            data_main.append('password'     , $("#pass").val());
            data_main.append('repassword'   , $("#repass").val());

            data_main.append('nama_user'    , $("#nama_user").val());
            data_main.append('tlp_user'     , $("#tlp_user").val());
            data_main.append('alamat_user'  , $("#alamat_user").val());

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/insert_user";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_user').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/data_user");?>");
            } else {
                $("#msg_id_tipe_user").html(detail_msg.id_tipe_user);
                $("#msg_email_user").html(detail_msg.email_user);
                $("#msg_username").html(detail_msg.username);
                $("#msg_pass").html(detail_msg.pass);
                $("#msg_repass").html(detail_msg.repass);
                $("#msg_nama_user").html(detail_msg.nama_user);
                $("#msg_tlp_user").html(detail_msg.tlp_user);
                $("#msg_alamat_user").html(detail_msg.alamat_user);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){

            $("#email_user").val("");
            $("#username").val("");
            $("#pass").val("");
            $("#repass").val("");
            $("#nama_user").val("");
            $("#tlp_user").val("");
            $("#alamat_user").val("");

            $("#msg_id_tipe_user").html("");
            $("#msg_email_user").html("");
            $("#msg_username").html("");
            $("#msg_pass").html("");
            $("#msg_repass").html("");
            $("#msg_nama_user").html("");
            $("#msg_tlp_user").html("");
            $("#msg_alamat_user").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_user--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_user_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_email").val("");
            $("#_username").val("");
            $("#_nama_user").val("");
            $("#_tlp_user").val("");
            $("#_alamat_user").val("");

            $("#_msg_id_tipe_user").html("");
            $("#_msg_email").html("");
            $("#_msg_username").html("");
            // $("#_msg_pass").html("");
            // $("#_msg_repass").html("");
            $("#_msg_nama_user").html("");
            $("#_msg_tlp_user").html("");
            $("#_msg_alamat_user").html("");
        }

        function update_user(id_user) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_user', id_user);

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_user);
                    $("#update_user").modal('show');
                }
            });
        }

        function set_val_update(res, id_user) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_user;

                $("#_id_tipe_user").val(list_data.id_tipe_user);
                $("#_email_user").val(list_data.email_user);
                $("#_username").val(list_data.username_user);
                
                $("#_nama_user").val(list_data.nama_user);
                $("#_tlp_user").val(list_data.tlp_user);
                $("#_alamat_user").val(list_data.alamat_user);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_user_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_user--------------------------//
    //=========================================================================//
        $("#btn_update_user").click(function() {
            var data_main = new FormData();
            data_main.append('id_user', id_cache);

            data_main.append('id_tipe_user' , $("#_id_tipe_user").val());
            data_main.append('email_user'   , $("#_email_user").val());
            data_main.append('username'     , $("#_username").val());

            data_main.append('nama_user'    , $("#_nama_user").val());
            data_main.append('tlp_user'     , $("#_tlp_user").val());
            data_main.append('alamat_user'  , $("#_alamat_user").val());

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/update_user";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_user').modal('toggle');
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/data_user");?>");
                clear_form_update();
            } else {
                $("#_msg_id_tipe_user").html(detail_msg.id_tipe_user);
                $("#_msg_email_user").html(detail_msg.email_user);
                $("#_msg_username").html(detail_msg.username);

                $("#_msg_nama_user").html(detail_msg.nama_user);
                $("#_msg_tlp_user").html(detail_msg.tlp_user);
                $("#_msg_alamat_user").html(detail_msg.alamat_user);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_user--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------user_delete--------------------------//
    //=========================================================================//

        function method_delete(id_user){
            var data_main = new FormData();
            data_main.append('id_user', id_user);

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/delete_user";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_user(id_user) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        method_delete(id_user);
                        // swal.close();
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/data_user");?>");
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------user_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function() {
            var data_main = new FormData();
            data_main.append('id_user', id_ch_cache);

            data_main.append('password'     , $("#ch_pass").val());
            data_main.append('repassword'   , $("#ch_repass").val());

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/change_pass_user";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_pass_new(res);
                    // response_change_pass(res, id_user_op);
                }
            });
        });

        function response_pass_new(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/data_user");?>");

                clear_ch_pass();
                $('#change_pass_modal').modal('toggle');;
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
                // console.log(detail_msg);
                $("#_msg_ch_pass").html(detail_msg.password);
                $("#_msg_ch_repass").html(detail_msg.repassword);
            }
        }

        function ch_pass(id_user) {
            $('#change_pass_modal').modal('show');
            id_ch_cache = id_user;
        }

        function clear_ch_pass(){
            $("#ch_pass").val("");
            $("#ch_repass").val("");
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------dasabled_user------------------------//
    //=========================================================================//
        function disabled_user(id_user) {
            var data_main = new FormData();
            data_main.append('id_user', id_user);

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/disabled_user/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_activeated(res);
                }
            });
        }
          
    //=========================================================================//
    //-----------------------------------dasabled_user------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------activate_user------------------------//
    //=========================================================================//

        function active_user(id_user) {
            
            var data_main = new FormData();
            data_main.append('id_user', id_user);

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/activate_user/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_activeated(res);
                }
            });
                       
        }

        function response_activeated(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/data_user");?>");
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------activate_user------------------------//
    //=========================================================================//
</script>