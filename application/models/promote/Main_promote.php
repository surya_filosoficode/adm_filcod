<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_promote extends CI_Model{
    
    public function get_promote_all($where){
    	$this->db->join("article_main am", "am.id_article_main = pa.id_article_pr");
    	$data = $this->db->get_where("pr_article pa", $where);
    	return $data->result();
    }

    public function get_promote_pg_home_all($where){
    	$this->db->join("product_jenis pj", "pph.id_jenis = pj.id_jenis");
        $this->db->order_by("pph.r_num_pr","ASC");
    	$data = $this->db->get_where("pr_page_home pph", $where);
    	return $data->result();
    }

    public function get_promote_pg_home_each($where){
    	$this->db->join("product_jenis pj", "pph.id_jenis = pj.id_jenis");
    	$data = $this->db->get_where("pr_page_home pph", $where);
    	return $data->row_array();
    }

}
?>