-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Mei 2020 pada 18.43
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fc`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin`(`id_tipe_admin` VARCHAR(3), `email` TEXT, `username` VARCHAR(15), `password` TEXT, `nama_admin` VARCHAR(100), `nip_admin` VARCHAR(64)) RETURNS varchar(12) CHARSET latin1
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where SUBSTR(id_admin, 3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where SUBSTR(id_admin, 3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
    set fix_key_user = concat("AD",SUBSTR(last_key_user,3,10)+1);  
  END IF;
  
  insert into admin values(fix_key_user, id_tipe_admin, email, username, password, '0', nama_admin, nip_admin, '0');
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_image`(`title_img` VARCHAR(64), `path_img` TEXT, `file_img` TEXT, `category_img` TEXT, `date_img` DATETIME, `owner_img` VARCHAR(12), `tipe_owner_img` INT, `jenis_img` VARCHAR(32)) RETURNS varchar(15) CHARSET latin1
BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
  
  select count(*) into count_row_user from m_img 
  	where left(id_img, 8) = left(NOW()+0, 8);
        
  select id_img into last_key_user from m_img
  	where left(id_img, 8) = left(NOW()+0, 8)
  	order by id_img desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat(left(NOW()+0, 8),"0000001");
  else
    set fix_key_user = concat(left(NOW()+0, 8), LPAD(right(last_key_user, 7) + 1, 7, '0'));
  END IF;
  
  insert into m_img values(fix_key_user, title_img, path_img, file_img, category_img, jenis_img, date_img, owner_img, tipe_owner_img);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_user`(`id_tipe_user` VARCHAR(3), `email_user` TEXT, `username` VARCHAR(32), `tlp_user` VARCHAR(13), `nama_user` VARCHAR(64), `alamat_user` TEXT, `password` TEXT) RETURNS varchar(15) CHARSET latin1
BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
  
  select count(*) into count_row_user from user 
  	where left(id_user, 8) = left(NOW()+0, 8);
        
  select id_user into last_key_user from user
  	where left(id_user, 8) = left(NOW()+0, 8)
  	order by id_user desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat(left(NOW()+0, 8),"0000001");
  else
    set fix_key_user = concat(left(NOW()+0, 8), LPAD(right(last_key_user, 7) + 1, 7, '0'));
  END IF;
  
  insert into user values(fix_key_user, id_tipe_user, email_user, username, tlp_user, nama_user, alamat_user, password, '0', '0');
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(12) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('AD2019110001', '0', 'suryahanggara@gmail.com', 'surya', '21232f297a57a5a743894a0e4a801fc3', '1', 'surya', '20190001', '0'),
('AD2019110002', '0', 'sudirman@gmail.com', 'sudirman', '21232f297a57a5a743894a0e4a801fc3', '0', 'sudirman', '7838372', '0'),
('AD2019110003', '0', 'dimas@gmail.comx', 'ara', '636bfa0fb2716ff876f5e33854cc9648', '1', 'ara', 'a', '0'),
('AD2019120001', '1', 'vasa@gmail.com', 'vasa', '21232f297a57a5a743894a0e4a801fc3', '1', 'vasa', 'a', '0'),
('AD2019120002', '0', 'didi@gmail.com', 'didi', '21232f297a57a5a743894a0e4a801fc3', '1', 'didi', 'a', '0'),
('AD2020010001', '0', 'surya', 'surya', 'surya', '1', 'asdsad', '1', '0'),
('AD2020010002', '0', 'suryahanggaraxxx@gmail.com', 'donix', '73acd9a5972130b75066c82595a1fae3', '1', 'asdsad', 'a', '0'),
('AD2020030001', '0', 'suryadimas@gmail.com', 'suryaxx', 'aff8fbcbf1363cd7edc85a1e11391173', '1', '0', 'surya', '0'),
('AD2020030002', '0', 'disa@gmail.com', 'disa', '21232f297a57a5a743894a0e4a801fc3', '0', 'disa', 'a', '0'),
('AD2020030003', '0', 'suryahanggarasss@gmail.com', 'suryasss', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya haggaras', '1245', '0'),
('AD2020030004', '1', 'tosni@gmail.com', 'tosni', 'e94c060979a16eca4a64e660d31db640', '1', 'tosni', 'a', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `article_jenis`
--

CREATE TABLE IF NOT EXISTS `article_jenis` (
  `id_jenis_article` varchar(32) NOT NULL,
  `nama_jenis_article` varchar(64) NOT NULL,
  `parent_jenis_article` varchar(32) NOT NULL,
  `is_delete_jenis_article` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_jenis_article`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `article_jenis`
--

INSERT INTO `article_jenis` (`id_jenis_article`, `nama_jenis_article`, `parent_jenis_article`, `is_delete_jenis_article`) VALUES
('AJ2020041600016', 'grand opening', '0', '0'),
('AJ2020041600017', 'promotion', '0', '0'),
('AJ2020041600018', 'flash sale', '0', '0'),
('AJ2020041600019', 'work from home', '0', '0'),
('AJ2020041600020', 'stay at home', '0', '0'),
('AJ2020041600021', 'article produk', '0', '0');

--
-- Trigger `article_jenis`
--
DROP TRIGGER IF EXISTS `before_insert_article_jenis`;
DELIMITER //
CREATE TRIGGER `before_insert_article_jenis` BEFORE INSERT ON `article_jenis`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'article_jenis';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'article_jenis';
    
    SET fix_key_user = concat("AJ",left(NOW()+0, 8),"",LPAD(rowcount_next, 5, '0'));
    
    SET NEW.id_jenis_article = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `article_main`
--

CREATE TABLE IF NOT EXISTS `article_main` (
  `id_article_main` varchar(32) NOT NULL,
  `title_article` text NOT NULL,
  `tipe_article` varchar(64) NOT NULL,
  `category_article` text NOT NULL,
  `tag_article` text NOT NULL,
  `main_img_article` text NOT NULL,
  `content_article` text NOT NULL,
  `create_admin_article` varchar(12) NOT NULL,
  `create_date_article` datetime NOT NULL,
  `status_check_article` enum('0','1') NOT NULL,
  `admin_check_article` varchar(12) NOT NULL,
  `date_check_article` datetime NOT NULL,
  `status_post_article` enum('0','1') NOT NULL,
  `admin_post_article` varchar(12) NOT NULL,
  `date_post_article` datetime NOT NULL,
  `is_delete_article` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_article_main`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `article_main`
--

INSERT INTO `article_main` (`id_article_main`, `title_article`, `tipe_article`, `category_article`, `tag_article`, `main_img_article`, `content_article`, `create_admin_article`, `create_date_article`, `status_check_article`, `admin_check_article`, `date_check_article`, `status_post_article`, `admin_post_article`, `date_post_article`, `is_delete_article`) VALUES
('ARC202004160000000021', 'BERDIKARI.! bersama Filosofi_code (Grand Opening)', 'promosi', '["grand opening","promotion","flash sale","work from home","article produk"]', '[]', 'base_url/assets/img/all_img/img_20200430101245_0.png', '<section class="single_blog_area section-padding-100">\n            <div class="container">\n                <div class="row justify-content-center">\n                    <div class="col-12 col-lg-12">\n                        <div class="row no-gutters">\n\n                            <!-- Single Post -->\n                            <div class="col-12">\n                                <div class="single-post">\n                                    <!-- Post Content -->\n                                    <div class="post-content">\n                                        <p style="text-align: center;"><img src="base_url/assets/img/all_img/img_20200501051952_0.png" style="width: 783px;"><br></p>\n                                        <br>\n\n                                        <p class="MsoNormal">Jreng-jreng selamat datang di pembukaan perdana website kami filosoficode.com. untuk kata-kata pertama ini perkenankanlah kami untuk mengucapkan rasa syukur kepada Allah SWT tentang terbangunnya website ini. Tak lupa kami juga mengucapkan rasa Terimakasih kami kepada semua pihak yang sudah memberikan dukungan, baik itu materi ataupun moril kepada kami sehingga website ini dapat dipublikasikan sesuai dengan harapan. Dan semoga informasi yang ada di website ini kedepannya dapat bermanfaat bagi siapapun yang membutuhkan.</p>\n\n\n                                        <p class="MsoNormal">Sebelum melanjutkan pembahasan tentang\n                                        artikel pertama kami yang berjudul BERDIKARI BERSAMA FILOSOFI_CODE, pasti\n                                        kalian bertanya-tanya <b>Apa sih Filosofi_Code Itu..?</b></p>\n\n                                        <p><br></p>\n                                        <p class="MsoNormal"><b><span style="font-size:20pt;line-height:107%">Filosofi_code adalah …</span></b></p>\n\n                                        <p class="MsoNormal" style="text-indent:.5in; margin-top: 30px;">\n                                        <img src="base_url/assets/img/all_img/img_20200501055623_0.png" style="width: 175px; height: 175px; float: left; margin-right: 40px; margin-left: 40px;margin-bottom: 20px;">\n                                        Filosofi_code\n                                        adalah start-up yang bergerak dibidang pembuatan website, aplikasi dan desain,\n                                        baik itu <i>custom</i> atau produk original\n                                        kami sendiri. Secara <i>brand</i>, Filosofi_code\n                                        dicetuskan pada tanggal 1 mei 2019, namun saat itu fokus kami adalah untuk berkarya\n                                        secara <i>freelance project</i>. Namun seiring\n                                        berjalannya waktu, keinginan kami untuk berkembang dan keluar dari ‘zona nyaman’,\n                                        akhirnya kami sepakat untuk membuat website dengan nama <b>filosoficode.com.</b></p>\n\n                                        <p class="MsoNormal">Sedangkan filosoficode.com\n                                        merupakan media untuk mempresentasikan, pun juga mempromosikan produk\n                                        filosofi_code dan partner-partner kerja kami. Tapi bukan hanya itu, selanjutnya\n                                        kami juga akan menyajikan informasi seputar tehnologi dalam berita, tutorial\n                                        hingga link <i>download</i> aplikasi yang\n                                        kalian butuhkan.</p>\n\n                                        <p><br></p>\n                                        <p class="MsoNormal"><b><span style="font-size:20pt;line-height:107%">Apa fokus produk dari filosofi_code .?</span></b></p>\n\n                                        <p class="MsoNormal">Fokus produk filosofi_code\n                                        adalah dibidang jasa pembuatan website, aplikasi baik itu android atau desktop dan desain. Saat ini terdapat beberapa jenis produk, yaitu:</p><br>\n\n                                        <p style="padding-right: 30px; padding-left: 30px;">\n                                            </p><p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto; text-indent:-.25in;mso-list:l0 level1 lfo1;tab-stops:.5in 1.5in 2.0in 2.5in 3.0in 265.6pt; padding-right: 30px; padding-left: 30px;">\n                                                <!--[if !supportLists]-->\n                                                <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                    <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">\n                                                    </span>\n                                                </span><!--[endif]--> \n                                                Website desain. <i>Output</i> pada katagori ini adalah desain website statis <i>include </i>beserta<i> source code</i> desainnya. Contoh: desain website admin, desain website berita, desain website profil perusahaan atau perseorangan dan banyak lainnya.\n                                            </p>\n\n                                            <p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto; text-indent:-.25in;mso-list:l0 level1 lfo1;tab-stops:.5in 1.5in 2.0in 2.5in 3.0in 265.6pt; padding-right: 30px; padding-left: 30px;">\n                                                <!--[if !supportLists]-->\n                                                <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                    <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">\n                                                    </span>\n                                                </span><!--[endif]--> Website Applikasi. Sedangkan <i>output</i> pada kategori aplikasi, yaitu desain aplikasi hingga fungsi aplikasi yang bisa disesuai dengan keinginan user <i>include </i>beserta<i> source code full </i>aplikasi. Contoh: website pencatatan inventori, website pencatatan keuangan, website analisa sentiment, website jual beli, website pemesanan tiket dan lain sebagainya.\n                                            </p>\n\n                                            <p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto; text-indent:-.25in;mso-list:l0 level1 lfo1;tab-stops:.5in 1.5in 2.0in 2.5in 3.0in 265.6pt; padding-right: 30px; padding-left: 30px;">\n                                                <!--[if !supportLists]-->\n                                                <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                    <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">\n                                                    </span>\n                                                </span><!--[endif]--> Android Desain, produk dalam katagori ini kami hanya bataskan pada pembuatan desain UI/UX atau desain aplikasi dalam bentuk <i>source code</i>\n                                            </p>\n\n                                            <p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto; text-indent:-.25in;mso-list:l0 level1 lfo1;tab-stops:.5in 1.5in 2.0in 2.5in 3.0in 265.6pt; padding-right: 30px; padding-left: 30px;">\n                                                <!--[if !supportLists]-->\n                                                <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                    <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">\n                                                    </span>\n                                                </span><!--[endif]--> Aplikasi Android. <i>Output</i> pada katagori ini sama dengan katagori website aplikasi. Yaitu desain aplikasi hingga fungsi aplikasi yang bisa disesuai dengan keinginan user <i>include </i>beserta<i> source code full </i>aplikasi. Contoh: aplikasi booking, aplikasi penjadwalan, aplikasi pencatatan keuangan, dan lain sebagainya.\n                                            </p>\n\n                                            <p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto; text-indent:-.25in;mso-list:l0 level1 lfo1;tab-stops:.5in 1.5in 2.0in 2.5in 3.0in 265.6pt; padding-right: 30px; padding-left: 30px;">\n                                                <!--[if !supportLists]-->\n                                                <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                    <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">\n                                                    </span>\n                                                </span><!--[endif]-->  Aplikasi Desktop. <i>Output</i> pada katagori ini sama dengan katagori aplikasi android. Yaitu\n                                                desain aplikasi hingga fungsi aplikasi bisa disesuai dengan keinginan user,\n                                                namun bedanya pada katagori ini produk hanya di berikan dalam bentuk <i>installer</i> atau <i>file.exe</i>. Contoh: aplikasi akuntansi, aplikasi kasir, aplikasi\n                                                pencatatan keuangan dan lain sebagainya.\n                                            </p>\n\n                                            <p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto; text-indent:-.25in;mso-list:l0 level1 lfo1;tab-stops:.5in 1.5in 2.0in 2.5in 3.0in 265.6pt; padding-right: 30px; padding-left: 30px;">\n                                                <!--[if !supportLists]-->\n                                                <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                    <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">\n                                                    </span>\n                                                </span><!--[endif]-->  Desain. Produk pada katagori ini meliputi desain\n                                                kartu nama, undangan pernikahan, foto <i>vector</i>,\n                                                logo, bener iklan, sampul buku, proposal pengajuan, <i>power point,</i> dan banyak lainnya.\n                                            </p>\n\n                                            <p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto; text-indent:-.25in;mso-list:l0 level1 lfo1;tab-stops:.5in 1.5in 2.0in 2.5in 3.0in 265.6pt; padding-right: 30px; padding-left: 30px;">\n                                                <!--[if !supportLists]-->\n                                                <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                    <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: " times="" new="" roman";"="">\n                                                    </span>\n                                                </span><!--[endif]--> Jasa <i>Maintenance</i>\n                                                Website dan Aplikasi, jasa ini dimaksudkan hanya untuk website dan aplikasi\n                                                yang dikembangkan oleh filosofi_code saja.\n                                            </p>\n                                        <p></p><br>\n\n                                        <p class="MsoNormal">Nah,\n                                        jadi itulah beberapa produk dan jasa yang kami tawarkan. Untuk penjelasan lebih\n                                        jelas, tentang harga dan bagaimana cara pemesanannya, silahkan hubungi kami -- <a href="http://localhost:8080/filosoficode/page/contact">disini</a> --.</p>\n                                        <br>\n\n                                        <p class="MsoNormal"><b><span style="font-size:20pt;line-height:107%">BERDIKARI BERSAMA FILOSOFI_CODE .?</span></b></p>\n\n\n                                        <p class="MsoNormal">Kembali pada judul artikel\n                                        pertama kami “<i>Berdikari Bersama\n                                        Filosofi_code</i>”. Berdikari merupakan istilah yang digunakan oleh salah satu\n                                        bapak pendiri bangsa ini, yaitu Ir. Soekarno pada awal kemerdekaan Indonesia. Maksud\n                                        dari kata Berdikari ialah “Berdiri diatas kaki sendiri”. Nah, sudah tau kan\n                                        maksudnya..?, pastilah sudah, karna kalian memang pintar.</p>\n\n                                        <p style="text-align: center;"><img src="base_url/assets/img/all_img/img_20200502012722_0.png" style="width: 545px; height: 306.49px;"><br></p>\n\n                                        <p class="MsoNormal">Tapi tunggu dulu, yang kami\n                                        maksud dengan berdikari ini bukanlah segala sesuatu harus dikerjakan dan\n                                        dihasilkan oleh kemampuan sendiri, tidak seperti itu.</p>\n                                        \n                                        <!-- blockquote -->\n                                        \n                                            <blockquote class="fancy-blockquote">\n                                                <span class="quote playfair-font">“</span>\n                                                <h5 class="mb-4">“<i>Come on you are not a superman right?”</i></h5>\n                                            </blockquote>\n                                        \n\n                                        \n                                        <p class="MsoNormal">Jadi\n                                        yang kami maksud dengan berdikari ini, ialah kita pasti membutuhkan manusia\n                                        lain untuk berinteraksi dan berkembang. Tentu kita tidak bisa meniadakan peran\n                                        manusia, content, dan produk dari bangsa lain contohnya. Tapi menurut kami\n                                        alangkah baiknya jika produk, content dan pemikiran yang sumbernya dari luar\n                                        itu jangan ditelan mentah-mentah dan digunakan begitu saja. Tapi juga harus\n                                        dipelajari, diteliti dan diolah supaya kita bisa menciptakan sesuatu yang baru\n                                        yang dipastikan itu cocok untuk kita gunakan sendiri.</p>\n\n                                        <p class="MsoNormal">Melalui\n                                        artikel ini, kami mengajak kalian semua yang ingin mengembangkan diri untuk\n                                        bersama-sama menciptakan produk, jasa, dan content yang asli hasil karya bangsa\n                                        sendiri dengan kualitas yang tidak kalah dengan bangsa maju itu dan yang paling\n                                        penting, “<i>Tidak menghilangkan kepribadian\n                                        asli diri kalian sendiri</i>”. Dan untuk kalian yang punya jasa, produk atau\n                                        ide yang original buatan kalian sendiri ayo gabung bareng kami di <i>partnership</i> filosofi_code. Nah untuk tau\n                                        apa itu <i>partnership</i> filosofi_code,\n                                        ini dia penjelasannya.</p>\n\n                                        <br>\n\n                                        <p class="MsoNormal"><b><span style="font-size:20pt;line-height:107%">Sekilas tentang <i>partnership</i> filosofi_code</span></b></p>\n                                        <br>\n\n                                        <p style="text-align: center; "><img src="base_url/assets/img/all_img/img_20200502013724_0.png" style="width: 736.5px; height: 234.814px;"><br></p>\n\n                                        <p class="MsoNormal">Program <i>partnership</i> filosofi_code ialah program yang bisa diikuti oleh\n                                        siapa saja. Komitmen kami sejak awal ialah “<i>no\n                                        limited for ide</i>”, jadi disini kalian bisa menjadi apapun dan membuat apapun\n                                        produk yang kalian inginkan. Jika kalian pandai mempromosikan, kalian bisa\n                                        menjadi reseler kami, jika kalian memiliki produk atau jasa kalian bisa\n                                        menampilkannya pada website kami sebagai promosi, dan jika kalian memiliki\n                                        keahlian menulis kalian bisa membagi artikel tentang keahlian kalian melalui\n                                        website kami, bahkan jika kalian tidak memiliki keahlian, dan hanya memiliki\n                                        pengalaman hidup, kalian bagi pengalaman itu melalui website kami (tapi dengan\n                                        catatan kami edit dulu sebelum di publikasikan). </p>\n\n                                        <p class="MsoNormal">\n                                            Nah, Penasaran bukan dengan\n                                            program <i>partnership </i>filosofi_code. Mau\n                                            tau gimana cara gabung bersama kami.? , eits sabar dulu, kami belum buatkan\n                                            artikelnya. Jadi sabar dulu ya. Untuk info selanjutnya, pantengin terus website\n                                            ini atau IG kami di <a href="javascript:window.open(''https://www.instagram.com/filosoficode/'')">@filosoficode</a>.\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>', 'AD2019110001', '2020-04-17 07:39:14', '1', 'AD2019110001', '2020-04-27 19:13:41', '1', 'AD2019110001', '2020-04-27 19:13:44', '0'),
('ARC202004160000000022', 'SUPER CASHBACK - Filosofi_code bagi-bagi angpau Lebaran', 'promosi', '["grand opening","promotion","flash sale","work from home"]', '[]', 'base_url/assets/img/all_img/img_20200430100850_0.png', '<section class="single_blog_area section-padding-100">\n            <div class="container">\n                <div class="row justify-content-center">\n                    <div class="col-12 col-lg-12">\n                        <div class="row no-gutters">\n\n                            <!-- Single Post -->\n                            <div class="col-12">\n                                <div class="single-post">\n                                    <!-- Post Content -->\n                                    <div class="post-content">\n                                        <p style="text-align: center; "><img src="base_url/assets/img/all_img/img_20200502025633_0.png" style="width: 662px;"><br></p>\n                                        <br>\n                                        <p class="MsoNormal">Halo kawan-kawan, sobat setia filosofi_code. Dijaman Corona seperti\n                                        sekarang ini pasti pada bingung ya mau ngapain. Nongkrong dilarang, mudik tidak\n                                        boleh, mau nekat mudik nanti sampai ditempat tujuan ditetapkan jadi orang dalam\n                                        pantauan pula. Mau jalan sama pacar pun juga pasti mikir (maklum banyak <i>café</i> dan restoran yang tutup, sekalipun\n                                        buka duduknya pisah jaraknya harus satu meter), pacaran ala corona memang\n                                        mengenaskan *hahahaha*.</p>\n\n                                        <p class="MsoNormal">Sudah lah lupakan mimpi buruk tentang pandemic ini. Semengenaskan apapun keadaan ini, Ingat quotes ini.\n\n                                        </p><blockquote class="fancy-blockquote">\n                                            <span class="quote playfair-font">“</span>\n                                            <h5 class="mb-4">“<i>Tersenumlah, maka dunia akan tersenyum kepadamu, Menangislah maka dunia akan menyiksamu”</i></h5>\n                                        </blockquote>\n\n                                        Maaf kalau quotes itu sedikit aneh, karena itu kami tambah sedikit untuk menghilankan kesan ATP pada tulisan ini. Eh, pasti mikir ya apa itu ATP.?. ATP bagi kami ialah “Amati Tiru Plek” alias “Copy Paste Without Edit”. Loh kok jadi ngelantur pikirannya, dari bahas quotes hingga ATP. Sudah kembali ke-topik bahasan lagi.<p></p>\n\n                                        <p class="MsoNormal">Berbicara masalah pacaran di-era corona ini menarik. Daripada\n                                        kalian bingung mau pacaran dengan cara bagaimana, kami punya ide untuk merubah\n                                        gaya pacaran dengan sistem baru, seru, saling menguntungkan, menambah wawasan\n                                        dan pastinya produktif (dengan catatan, asal tidak putus di tengah jalan). Berikut\n                                        ini cara pacaran di-era corona ala filosofi_code.</p>\n                                        \n                                        <br>\n                                        <p class="MsoListParagraphCxSpFirst" style="margin-left:18.25pt;mso-add-space: auto;text-indent:-.25in;mso-list:l1 level1 lfo1">\n                                            <b><span style="font-size:20.0pt;line-height:107%">\n                                                <!--[if !supportLists]-->1.\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">      \n                                                </span>\n                                                <!--[endif]-->Ajak pacarmu menulis artikel bersama.\n                                            </span></b>\n                                        </p>\n\n                                        <img src="base_url/assets/img/all_img/img_20200502032730_0.png" style="width: 579.869px;height: 326px;float: left;margin: 20px 20px 20px 10px;">\n                                        \n                                        <p class="MsoNormal">Kalian pasti tau, jika menulis merupakan salah satu media yang dapat\n                                        menyalurkan banyak hal yang ada dipikiran kalian. Bagi seorang introvert\n                                        kebanyakan menulis adalah salah satu cara untuk memperkenalkan diri mereka\n                                        kepada dunia, bagi mereka itu juga merupakan cara berkomunikasi dengan manusia\n                                        lain. Sedangkan bagi kaum intelektual, menulis merupakan cara yang tepat untuk\n                                        mencatat kamus ilmu mereka, dan juga media untuk mempublikasikan karya mereka\n                                        kepada dunia, namun ujungnya juga untuk mengembangkan diri. Bahkan untuk kalian\n                                        yang patah hati, menulis juga salah satu cara untuk “<i>Healing</i>” (menyembuhkan diri) dengan cara mengekspresikan perasaan\n                                        kalian.</p>\n                                        <p class="MsoNormal">Tapi tidak hanya soal itu, menulis jika di pandang dari sisi ekonomi juga\n                                        merupakan salah satu moda investasi loh. Entah itu kalian menulis artikel\n                                        ilimiah, fiksi atau novel sekalipun itu terserah kalian. Sebagai contoh,\n                                        seorang content writer tulisannya bisa dihargai hingga Rp. 100.000 per artikel\n                                        (dengan catatan ya, jika tulisannya memang bagus). Menulis juga tidak melulu\n                                        soal hal-hal <i>mainstream </i>serperti\n                                        cerita cinta, tutorial keahlian, cara ingin kaya dan tips penggunaan satu produk.\n                                        Namun nyatanya banyak tulisan-tulisan diluar contoh tersebut ternyata bisa loh\n                                        jadi sebuah karya fenomenal meskipun hanya dari pengalaman hidup. Sebut saja tulisan\n                                        yang berjudul “KKN Desa Penari” yang dipublikasikan oleh “Mr. Simple Man” di <i>twitter </i>yang menjadi trending topik\n                                        beberapa waktu lalu. luar biasanya lagi saat ini sudah muncul 3 film dari\n                                        tulisan tersebut (2 film dokumenter dan satu film yang diproduksi resmi). Nah lo,\n                                        keren gak itu. Bayangkan jika tulisan-mu dan pasangan-mu yang dijadikan film,\n                                        lebih keren kan pastinya.</p>\n                                        <p class="MsoNormal">Jadi ayo mulai ajak pasanganmu untuk menulis. Mulai dari hal-hal\n                                        sederhana saja, misal dari pengalaman hidup kalian, tips untuk melakukan\n                                        sesuatu hal atau bahkan membagi sedikit ilmu kalian melalui artikel\n                                        pembelajaran. Seru pastinya jika kamu dan pasangan mu dapat berbagi melaui\n                                        tulisan-tulisan kalian dan tentunya mendapatkan sedikit tambahan untuk <i>dating.</i></p>\n\n\n                                        <br>\n                                        <p class="MsoListParagraphCxSpFirst" style="margin-left:18.25pt;mso-add-space: auto;text-indent:-.25in;mso-list:l1 level1 lfo1">\n                                            <b><span style="font-size:20.0pt;line-height:107%">\n                                                <!--[if !supportLists]-->2.\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">      \n                                                </span>\n                                                <!--[endif]-->Membangun blog bersama pasanganmu.\n                                            </span></b>\n                                        </p>\n\n                                        <img src="base_url/assets/img/all_img/img_20200502032730_1.png" style="width: 579.869px;height: 326px;float: right;margin: 10px 10px 5px 30px;">\n\n                                        <p class="MsoNormal">Cara ke-2 ini merupakan cara mainstream yang bisa menjadi opsi untuk kalian memanfaatkan waktu di tengah musibah <i>lockdown</i>seperti sekarang. Membangun blog memang adalah cara yang ampuh untuk\n                                        memanfaatkan waktu. Pasti kalian sudah paham manfaat membuat blog, seperti yang\n                                        banyak kalian baca ketika mengalami kebingungan dan mencari tutorial, atau\n                                        kalian ingin mencari tips untuk suatu hal. Pasti pikiran kalian sudah mengerti\n                                        akan manfaatnya blog. Selain sebagai media untuk berbagi ilmu, pengalaman dan\n                                        penjualan produk, blog juga bisa dikatagorikan sebagai salah satu media investasi\n                                        yang mudah pun murah. Dimana sifat penghasilan blog adalah <i>unlimited </i>tergantung seberapa bagus <i>content </i>yang dihasilkan sehingga dapat menarik <i>user </i>untuk membaca <i>content </i>kalian. Keren bukan jika hal ini bisa kalian lakukan bersama dengan pasangan. Tentu hubungan kalian akan lebih menyenagkan juga produktif</p>\n\n\n\n\n                                        <p class="MsoNormal">Sekian dulu ya kawan, tips dari filosofi_code untuk\n                                        kalian yang ingin memanfaatkan waktu berdua dengan pasangan tapi tetap\n                                        produktif. Bukannya kami tidak punya ide lagi untuk melanjutkan tulisan ini,\n                                        atau kami tidak ingin membagi tips untuk kalian semua. Hanya saja sebenarnya\n                                        artikel ini kami maksutkan untuk menyampaikan info tentang promo yang sedang\n                                        berlangsung di filosofi_code. <i>By the way</i>\n                                        promo apa nih .? penasaran ya. Yauda langsung aja <i>check this out.</i></p>\n\n\n                                        <br>\n                                        <p class="MsoNormal"><b><span style="font-size:20.0pt;line-height:107%">CASH BACK 250K!!! dari filosofi_code</span></b></p>\n                                        <br>\n                                        <p style="text-align: center; "><img src="base_url/assets/img/all_img/img_20200502034328_0.png" style="width: 662px;"></p>\n                                        <br>\n\n                                        <p class="MsoNormal">Sebagai dukungan kami kepada pemerintah tentang himbauan <b>#dirumahaja</b>, sekaligus dukungan kami\n                                        kepada kalian yang ingin <i>go onlie</i>.  Kami memberikan <i>cash back</i> untuk kalian yang ingin mengembagkan blog atau website\n                                        bersama kami sebesar <b>250K</b>. Tapi promo\n                                        ini hanya berlaku pada beberapa produk kami yaitu :</p>\n\n                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l2 level1 lfo2;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Website berbasis <i>content </i>atau blog.\n                                        </p>\n\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo2;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Website desain dan website profil.\n                                        </p>\n\n                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l2 level1 lfo2;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Aplikasi untuk penelitian yang bernilai di bawah 5 Juta Rupiah.\n                                        </p>\n\n                                        <p class="MsoNormal">Dengan syarat sebagai berikut:</p>\n\n                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l0 level1 lfo3; padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Niat membangun website/aplikasi.\n                                        </p>\n\n                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l0 level1 lfo3; padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">   \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Memiliki komitmen bahwa “membangun website/aplikasi butuh\n                                            ketekunan, tidak sekali langsung jadi seperti menggoreng martabak”.\n                                        </p>\n\n                                        <p class="MsoNormal">Promo ini berlaku mulai <b>3\n                                        Mei 2020</b> sampai <b>18 Agustus 2020</b>\n                                        ya. Catatan, promo ini terbatas <b>Hanya untuk\n                                        18 orang pemesan pertama</b>. Jadi Ayo buruan <i>booking</i> jasa kami dengan cara hubungi kami – <a href="http://localhost:8080/filosoficode/page/about_us">disini</a> –, atau DM di\n                                        Instagram kami <a href="javascript:window.open(''https://www.instagram.com/filosoficode/'')">@filosoficode</a>  </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>', 'AD2019110001', '2020-04-17 07:47:19', '1', 'AD2019110001', '2020-04-27 19:13:48', '1', 'AD2019110001', '0000-00-00 00:00:00', '0');
INSERT INTO `article_main` (`id_article_main`, `title_article`, `tipe_article`, `category_article`, `tag_article`, `main_img_article`, `content_article`, `create_admin_article`, `create_date_article`, `status_check_article`, `admin_check_article`, `date_check_article`, `status_post_article`, `admin_post_article`, `date_post_article`, `is_delete_article`) VALUES
('ARC202004160000000023', 'Dirumah Aja Bareng Filosofi_code', 'berita', '["grand opening","work from home","stay at home"]', '[]', 'base_url/assets/img/all_img/img_20200430101245_1.png', '<section class="single_blog_area section-padding-100">\n            <div class="container">\n                <div class="row justify-content-center">\n                    <div class="col-12 col-lg-12">\n                        <div class="row no-gutters">\n\n                            <!-- Single Post -->\n                            <div class="col-12">\n                                <div class="single-post">\n                                    <!-- Post Content -->\n                                    <div class="post-content">\n                                        <p style="text-align: center; "><img src="base_url/assets/img/all_img/img_20200502094402_0.png" style="width: 763.808px; height: 429.188px;"><br></p>\n                                        <br>\n\n                                        <blockquote class="fancy-blockquote">\n                                            <span class="quote playfair-font">“</span>\n                                            <h5 class="mb-4">“<i>Stay At Home alias Dirumah saja atau Ndek omah ae”</i></h5>\n                                        </blockquote>\n\n                                        <p class="MsoNormal">Mungkin Quote itu yang akhir-akhir ini sering kalian dengar di berbagai\n                                        media di seluruh belahan dunia. Iya benar sekali, semua memang berubah sejak\n                                        negara api menyerang (virus corona mewabah maksudnya). Mengingat kecepatan\n                                        infeksinya dan begitu banyak korban yang berjatuhan dan mengguncang dunia\n                                        hingga menjadikan virus ini menjadi wabah internasional terparah sepanjang masa.\n                                        Terlepas dari segala perdebatan yang berkaitan dengan virus ini, tentang ini\n                                        sengaja dibuat oleh suatu kelompok atau bukan, yang jelas nyatanya ini virus\n                                        memang ada. Faktanya korban memang semakin banyak. Kita tidak tau apa makhluk\n                                        kecil itu ada di sekitar kita atau tidak.</p>\n\n                                        <p class="MsoNormal">Hanya saja demi kebaikan umat, ada baiknya kita tetap waspda,\n                                        menjaga diri, keluarga dan manusia lain disekitar kita. Oleh karena itu,\n                                        tetaplah tenang dan bijak dalam menyikapi wabah ini. Kendati virus ini\n                                        diinformasikan dapat disembuhkan sendiri dengan mengisolasi diri, tapi untuk\n                                        sementara ini ikuti saja aturan pemerintah terkait <i>physical distancing. </i>Bukan hanya korban yang berjatuhan, namun juga\n                                        ekonomi juga ikut terpuruk karena virus ini. Nah itu mengapa kita harus\n                                        mengikuti aturan <i>physical distancing</i> ini\n                                        untuk sementara waktu agar wabah corona ini cepat berlalu dan kehidupan kita\n                                        dapat kembali berjalan seperti dulu. Untuk kalian, berikut kami sampaikan\n                                        sedikit infromasi tentang virus corona.</p>\n\n\n                                        <br>\n                                        <p class="MsoNormal">\n                                            <b>\n                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;\n                                        mso-bidi-theme-font:minor-latin">Apa sih Corona itu.?\n                                                    <o:p></o:p>\n                                                </span>\n                                            </b>\n                                        </p>\n                                        <br>\n                                        <p style="text-align: center; "><img src="base_url/assets/img/all_img/img_20200502094707_0.png" style="width: 763.808px; height: 429.188px;"><br></p>\n                                        <br>\n                                        <p class="MsoNormal">\n                                            <i>Coronavirus</i> atau virus corona merupakan keluarga besar virus yang menyebabkan infeksi saluran pernapasan atas ringan hingga sedang, seperti penyakit <i>flu</i>. Banyak orang terinfeksi virus ini, setidaknya satu kali dalam hidupnya. Namun, beberapa jenis virus corona juga bisa menimbulkan penyakit yang lebih serius, seperti:\n                                        </p>\n                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l1 level1 lfo4;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Middle East Respiratory Syndrome (MERS-CoV).\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l1 level1 lfo4;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Severe Acute Respiratory Syndrome (SARS-CoV).\n                                        </p>\n                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l1 level1 lfo4;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Pneumonia.\n                                        </p>\n                                        <p class="MsoNormal">SARS yang muncul pada November 2002 di Tiongkok, menyebar ke\n                                        beberapa negara lain. Mulai dari Hongkong, Vietnam, Singapura, Indonesia,\n                                        Malaysia, Inggris, Italia, Swedia, Swiss, Rusia, hingga Amerika Serikat.\n                                        Epidemi SARS yang berakhir hingga pertengahan 2003 itu menjangkiti 8.098 orang\n                                        di berbagai negara. Setidaknya 774 orang mesti kehilangan nyawa akibat penyakit\n                                        infeksi saluran pernapasan berat tersebut. Sampai saat ini terdapat tujuh \n                                            <i>coronavirus</i> (HCoVs) yang telah\n                                        diidentifikasi, yaitu:\n                                        </p>\n                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->HCoV-229E.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->HCoV-OC43.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->HCoV-NL63.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->HCoV-HKU1.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->SARS-COV (yang menyebabkan sindrom pernapasan akut).\n                                        </p>\n                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->MERS-COV (sindrom pernapasan Timur Tengah).\n                                        </p>\n\n                                        <p class="MsoNormal">COVID-19 atau dikenal juga dengan Novel \n                                            <i>Coronavirus</i> (menyebabkan wabah pneumonia di kota Wuhan, Tiongkok\n                                        pada Desember 2019, dan menyebar ke negara lainnya mulai Januari 2020.\n                                        Indonesia sendiri mengumumkan adanya kasus covid 19 dari Maret 2020.\n                                            \n                                        </p>\n                                        <p class="MsoNormal">Sumber: \n                                            <span class="MsoHyperlink">\n                                                <span style="color: windowtext;">\n                                                    <a href="https://www.halodoc.com/kesehatan/coronavirus">\n                                                        <span style="color: windowtext;">halodoc.com/kesehatan/coronavirus</span>\n                                                    </a>\n                                                </span>\n                                            </span>\n                                        </p>\n\n                                        <br>\n\n                                        <p class="MsoNormal">\n                                            <b>\n                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Bahayanya dan cara penularan Covid19       \n                                                </span>\n                                            </b>\n                                        </p>\n                                        <p class="MsoNormal">\n                                            Bahaya virus corona atau Covid-19 yaitu transmisi\n                                        yang cepat dan lebih mudah dibandingkan wabah SARS yang pernah melanda dunia\n                                        pada tahun 2003, dikutip dari \n                                                <i>Financial\n                                        Times</i>. Penyebaran yang cepat ini membuat kasus positif corona di dunia\n                                        mencapai 935.957 per Kamis (2/4/2020) atau dalam kurun waktu 5 bulan atau sejak\n                                        kasus pertama ditemukan pada November 2019. Virus corona menyerang saluran\n                                        pernapasan manusia. Seseorang dapat terinfeksi dari penderita Covid-19.</p>\n                                        <p class="MsoNormal">\n                                            Penyakit ini dapat menyebar melalui tetesan kecil\n                                        (droplet) dari hidung atau mulut pada saat batuk atau bersin. Droplet tersebut\n                                        kemudian jatuh pada benda di sekitarnya. Kemudian jika ada orang lain menyentuh\n                                        benda yang sudah terkontaminasi dengan droplet tersebut, lalu orang itu\n                                        menyentuh mata, hidung atau mulut (segitiga wajah), maka orang itu dapat\n                                        terinfeksi Covid-19. Bisa juga seseorang terinfeksi Covid-19 ketika tanpa\n                                        sengaja menghirup droplet dari penderita. Inilah sebabnya mengapa kita penting\n                                        untuk menjaga jarak hingga kurang lebih satu meter dari orang yang sakit. Virus\n                                        baru ini memiliki gejala awal seperti demam, batuk, pilek, gangguan pernapasan,\n                                        sakit tenggorokan, letih, dan lesu. Dikutip dari BBC, pemeriksaan data oleh\n                                        Organisasi Kesehatan Dunia (WHO) dari 56.000 pasien menunjukkan 6 persen\n                                        memiliki gejala kritis seperti gangguan pada paru, septic shock hingga risiko\n                                        kematian. Sebanyak 14 persen mengalami gejala berat yaitu kesulitan bernapas\n                                        dan sesak napas. Sementara 80 persen lainnya memiliki gejala ringan seperti\n                                        demam, batuk dan beberapa memiliki pneumonia. Meski pasien yang memiliki risiko\n                                        meninggal hanya sekitar 6 persen, proporsi ini tak dapat disepelehkan. Kasus\n                                        meninggal karena Covid-19 di seluruh dunia mencapai 47.245 dengan jumlah\n                                        tertinggi berasal dari Italia yakni 13.155. Kasus meninggal terbanyak kedua terdapat\n                                        di Spanyol dengan jumlah 9.387. Cina, sebagai negara pertama yang mendeteksi\n                                        virus baru uni mencapat kasus meninggal sebanyak 3.312. Sementara di Indonesia,\n                                        kasus meninggal mencapai 157, menurut data Worldometers. Lansia disebut menjadi\n                                        salah satu kelompok yang sangat rentan dengan virus baru ini, termasuk mereka\n                                        yang menderita penyakit lain misalnya asma, diabetes, penyakit jantung hingga\n                                        tekanan darah tinggi. Kelompok ini berpotensi memiliki gejala berat hingga\n                                        kritis jika terinfeksi Covid-19. Data dari Cina juga menunjukkan bahwa pria\n                                        berisiko sedikit lebih tinggi meninggal akibat virus daripada wanita.</p>\n                                        <p class="MsoNormal">\n                                            Yang membuat virus ini lebih berbahaya karena tak\n                                        semua yang terinfeksi menunjukkan gejala serius. Bahkan ada yang mengalami\n                                        gejala ringan bahkan tanpa gejala atau silent carrier. Silent carrier ini sulit\n                                        dideteksi sebab hanya bisa diketahui hanya melalui pemeriksaan. Sementara\n                                        mereka yang tidak menunjukkan gejala, bisa saja berpikir bahwa dirinya sehat\n                                        dan beraktivitas seperti biasa. Padahal ia dapat menularkan virus corona ini\n                                        pada orang lain, baik di rumahnya maupun masyarakat umum lainnya, sehingga\n                                        penyebaran makin meluas. Sebanyak enam dari sepuluh kasus \n                                                <i>coronavirus</i> mungkin disebabkan oleh orang yang mengalami gejala\n                                        ringan atau tidak sama sekali, kata Weforum.org.</p>\n\n                                        <p class="MsoNormal">Sumber: \n                                            <span class="MsoHyperlink">\n                                                <span style="color: windowtext;">\n                                                    <a href="https://tirto.id/eKdF">\n                                                        <span style="color: windowtext;">tirto.id/eKdF</span>\n                                                    </a>\n                                                </span>\n                                            </span>\n                                        </p>\n\n                                        <br>\n                                        <p class="MsoNormal">\n                                            <b>\n                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Bagaimana gejala jikalau kalian terinfeksi Covid19\n                                                </span>\n                                            </b>\n                                        </p>\n                                        <br>\n                                        <p style="text-align: center; "><img src="base_url/assets/img/all_img/img_20200502094140_0.png" style="width: 689.5px; height: 525.744px;"><br></p>\n                                        <br>\n                                        <p class="MsoNormal">Virus corona bisa menimbulkan beragam gejala pada pengidapnya.\n                                        Gejala yang muncul ini bergantung pada jenis virus corona yang menyerang, dan\n                                        seberapa serius infeksi yang terjadi. Berikut beberapa gejala virus corona yang\n                                        terbilang ringan:</p>\n                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Hidung beringus.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Sakit kepala.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Batuk.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Sakit tenggorokan.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Demam.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Merasa tidak enak badan.\n                                        </p>\n                                        <p class="MsoNormal">Hal yang perlu ditegaskan, beberapa virus corona dapat menyebabkan\n                                        gejala yang parah. Infeksinya dapat berubah menjadi bronkitis dan pneumonia\n                                        (disebabkan oleh COVID-19), yang mengakibatkan gejala seperti:</p>\n\n                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Demam yang mungkin cukup tinggi bila pasien mengidap pneumonia.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        </span>\n                                            </span>\n                                            <!--[endif]-->Batuk dengan lendir.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Sesak napas.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Nyeri dada atau sesak saat bernapas dan batuk.\n                                        </p>\n                                        <p class="MsoNormal">Infeksi bisa semakin parah bila menyerang kelompok individu\n                                        tertentu. Contohnya, orang dengan penyakit jantung atau paru-paru, orang dengan\n                                        sistem kekebalan yang lemah, bayi, dan lansia.</p>\n\n                                        <br>\n                                        <p class="MsoNormal">\n                                            <b>\n                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Bagaimana cara pencegahan Covid19\n                                                </span>\n                                            </b>\n                                        </p>\n\n                                        <p class="MsoNormal">Sampai saat ini belum ada vaksin untuk mencegah infeksi virus\n                                        corona. Namun, setidaknya ada beberapa cara yang bisa dilakukan untuk\n                                        mengurangi risiko terjangkit virus ini. Berikut upaya yang bisa dilakukan:</p>\n                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Sering-seringlah mencuci tangan dengan sabun dan air selama 20 detik hingga bersih.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Hindari menyentuh wajah, hidung, atau mulut saat tangan dalam keadaan kotor atau belum dicuci.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Hindari kontak langsung atau berdekatan dengan orang yang sakit.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Hindari menyentuh hewan atau unggas liar.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Membersihkan dan mensterilkan permukaan benda yang sering digunakan. \n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Tutup hidung dan mulut ketika bersin atau batuk dengan tisu. Kemudian, buanglah tisu dan cuci tangan hingga bersih.           \n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Jangan keluar rumah dalam keadaan sakit.\n                                        </p>\n                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Kenakan masker dan segera berobat ke fasilitas kesehatan ketika mengalami gejala penyakit saluran napas.      \n                                        </p>\n                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">\n                                            <!--[if !supportLists]-->\n                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·\n                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: "Times New Roman";">        \n                                                </span>\n                                            </span>\n                                            <!--[endif]-->Selain itu, kamu juga bisa perkuat sistem\n                                            kekebalan tubuh dengan konsumsi vitamin dan suplemen sebagai bentuk pencegahan\n                                            dari virus ini.\n                                        </p>\n\n                                        <br>\n                                        <p class="MsoNormal">\n                                            <b>\n                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Kapan harus ke dokter\n                                                </span>\n                                            </b>\n                                        </p>\n                                        <p class="MsoNormal">Segera lakukan isolasi mandiri bila Anda mengalami gejala infeksi\n                                        virus Corona (COVID-19) seperti yang telah disebutkan di atas, terutama jika\n                                        dalam 2 minggu terakhir Anda berada di daerah yang memiliki kasus COVID-19 atau\n                                        kontak dengan penderita COVID-19. Setelah itu, hubungi hotline COVID-19 di 119\n                                        Ext. 9 untuk mendapatkan pengarahan lebih lanjut.</p>\n                                        <p class="MsoNormal">Bila Anda mungkin terpapar virus Corona tapi tidak mengalami\n                                        gejala apa pun, Anda tidak perlu memeriksakan diri ke rumah sakit, cukup\n                                        tinggal di rumah selama 14 hari dan membatasi kontak dengan orang lain. Bila\n                                        muncul gejala, baru lakukan isolasi mandiri dan tanyakan kepada dokter melalui\n                                        telepon atau aplikasi mengenai tindakan apa yang perlu Anda lakukan dan obat\n                                        apa yang perlu Anda konsumsi.</p>\n                                        <p class="MsoNormal">Sumber: \n                                            <span class="MsoHyperlink">\n                                                <span style="color: windowtext;">\n                                                    <a href="https://www.alodokter.com/virus-corona">\n                                                        <span style="color: windowtext;">www.alodokter.com/virus-corona</span>\n                                                    </a>\n                                                </span>\n                                            </span>\n                                        </p>\n\n                                        <p class="MsoNormal"><span class="MsoHyperlink">Sekian dulu kawan infromasi dari kami tentang corona. Ingat\n                                        ya kawan tahan diri untuk sementara waktu </span>“<i>Stay\n                                        At Home</i> alias Dirumah saja atau Ndek omah ae”. Jangan ngeyel, apalai kalau\n                                        daerah kalian termasuk kategori merah alias banyak orang yang terinveksi.\n\n                                        </p><p class="MsoNormal">Ok, Sampai jumpa di artikel filosofi_code selanjutnya. Tetap\n                                        pentengin <a href="http://localhost:8080/filosoficode/page/about_us">website kami</a> atau IG kami <a href="javascript:window.open(''https://www.instagram.com/filosoficode/'')">@filosoficode</a> untuk dapat informasi dan promo\n                                        terbaru.– Terimakasih --</p>\n                                        \n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>', 'AD2019110001', '2020-04-17 07:50:30', '1', 'AD2019110001', '2020-04-28 00:23:07', '1', 'AD2019110001', '0000-00-00 00:00:00', '0'),
('ARC202004270000000024', 'testss', 'berita', '["grand opening","promotion","work from home","stay at home","article produk"]', '["fre","gf","g","er","gr","th","rt"]', 'base_url/assets/img/all_img/img_20200423124810_1.png', '<p><img src="base_url/assets/img/all_img/img_20200417111846_1.png" xss=removed>sip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku inisip apik kan tulisanku ini<br></p>', 'AD2019110001', '2020-04-27 18:44:47', '0', 'AD2019110001', '2020-05-02 17:47:43', '0', 'AD2019110001', '2020-05-02 17:47:47', '0'),
('ARC202004270000000025', 'test2', 'suka-suka', '["grand opening","promotion","flash sale","work from home","stay at home"]', '["jos","apiklah","mantuo"]', 'base_url/assets/img/all_img/img_20200417111846_1.png', '<p><img src="base_url/assets/img/all_img/img_20200417111846_1.png" xss=removed>sip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos banger</p>', 'AD2019110001', '2020-04-27 18:47:09', '1', 'AD2019110001', '2020-04-30 02:38:48', '0', 'AD2019110001', '2020-05-02 17:47:50', '0');
INSERT INTO `article_main` (`id_article_main`, `title_article`, `tipe_article`, `category_article`, `tag_article`, `main_img_article`, `content_article`, `create_admin_article`, `create_date_article`, `status_check_article`, `admin_check_article`, `date_check_article`, `status_post_article`, `admin_post_article`, `date_post_article`, `is_delete_article`) VALUES
('ARC202004270000000026', 'test2', 'tutorial', '["promotion","flash sale","work from home"]', '["ya","jos","ance","bagus"]', 'base_url/assets/img/all_img/img_20200423124632_1.png', '<p><img src="base_url/assets/img/all_img/img_20200417111846_1.png" xss=removed>sip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos banger</p>', 'AD2019110001', '2020-04-27 18:48:13', '1', 'AD2019110001', '2020-04-30 02:38:50', '0', '0', '0000-00-00 00:00:00', '0'),
('ARC202004280000000027', 'test', 'berita', '["grand opening","promotion","flash sale","work from home"]', '["bagus","sekali"]', 'base_url/assets/img/all_img/img_20200423124632_1.png', '<p><img src="base_url/assets/img/all_img/img_20200417111846_1.png" xss="removed" style="width: 385.6px; height: 241px; float: left;">sip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos bangersip lah jos banger</p>', 'AD2019110001', '2020-04-28 00:16:19', '1', 'AD2019110001', '2020-04-30 02:38:51', '0', '0', '0000-00-00 00:00:00', '0');

--
-- Trigger `article_main`
--
DROP TRIGGER IF EXISTS `before_insert_article_main`;
DELIMITER //
CREATE TRIGGER `before_insert_article_main` BEFORE INSERT ON `article_main`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'article_main';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'article_main';
    
    SET fix_key_user = concat("ARC",left(NOW()+0, 8),"",LPAD(rowcount_next, 10, '0'));
    
    SET NEW.id_article_main = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `article_tipe`
--

CREATE TABLE IF NOT EXISTS `article_tipe` (
  `id_art_tipe` varchar(32) NOT NULL,
  `nama_art_tipe` varchar(64) NOT NULL,
  `is_del_art_tipe` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_art_tipe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `article_tipe`
--

INSERT INTO `article_tipe` (`id_art_tipe`, `nama_art_tipe`, `is_del_art_tipe`) VALUES
('AT2020042700020', 'berita', '0'),
('AT2020042700021', 'promosi', '0'),
('AT2020042700022', 'tutorial', '0'),
('AT2020042700023', 'informasi', '0'),
('AT2020042700024', 'produk', '0');

--
-- Trigger `article_tipe`
--
DROP TRIGGER IF EXISTS `before_insert_article_tipe`;
DELIMITER //
CREATE TRIGGER `before_insert_article_tipe` BEFORE INSERT ON `article_tipe`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'article_tipe';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'article_tipe';
    
    SET fix_key_user = concat("AT",left(NOW()+0, 8),"",LPAD(rowcount_next, 5, '0'));
    
    SET NEW.id_art_tipe = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `index_number`
--

CREATE TABLE IF NOT EXISTS `index_number` (
  `id` varchar(32) NOT NULL,
  `id_index` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `index_number`
--

INSERT INTO `index_number` (`id`, `id_index`) VALUES
('article_jenis', '21'),
('article_main', '27'),
('article_tipe', '24'),
('brand', '18'),
('m_img_category', '29'),
('product', '18'),
('product_jenis', '28'),
('toko', '7');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_img`
--

CREATE TABLE IF NOT EXISTS `m_img` (
  `id_img` varchar(15) NOT NULL,
  `title_img` varchar(64) NOT NULL,
  `path_img` text NOT NULL,
  `file_img` text NOT NULL,
  `category_img` text NOT NULL,
  `jenis_img` varchar(32) NOT NULL,
  `date_img` datetime NOT NULL,
  `owner_img` varchar(12) NOT NULL,
  `tipe_owner_img` int(11) NOT NULL,
  PRIMARY KEY (`id_img`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_img`
--

INSERT INTO `m_img` (`id_img`, `title_img`, `path_img`, `file_img`, `category_img`, `jenis_img`, `date_img`, `owner_img`, `tipe_owner_img`) VALUES
('202004300000001', 'promo_aplikasi', 'assets/img/all_img/', 'img_20200430100850_0.png', '["filosofi_code promo","grand opening"]', 'header', '2020-04-30 10:08:50', 'AD2019110001', 0),
('202004300000002', 'grand_opening', 'assets/img/all_img/', 'img_20200430101245_0.png', '["filosofi_code promo","grand opening"]', 'header', '2020-04-30 10:12:45', 'AD2019110001', 0),
('202004300000003', 'stay_at_home', 'assets/img/all_img/', 'img_20200430101245_1.png', '["work from home","stay at home"]', 'header', '2020-04-30 10:12:45', 'AD2019110001', 0),
('202004300000004', 'neo_green', 'assets/img/all_img/', 'img_20200430103853_0.png', '["card_name"]', 'product', '2020-04-30 10:38:53', 'AD2019110001', 0),
('202004300000005', 'neo_green', 'assets/img/all_img/', 'img_20200430103853_1.png', '["card_name"]', 'product', '2020-04-30 10:38:53', 'AD2019110001', 0),
('202004300000006', 'sky_blue', 'assets/img/all_img/', 'img_20200430104243_0.png', '["card_name"]', 'product', '2020-04-30 10:42:43', 'AD2019110001', 0),
('202004300000007', 'sky_blue', 'assets/img/all_img/', 'img_20200430104243_1.png', '["card_name"]', 'product', '2020-04-30 10:42:43', 'AD2019110001', 0),
('202004300000008', 'colorful_design', 'assets/img/all_img/', 'img_20200430104521_0.png', '["card_name"]', 'product', '2020-04-30 10:45:21', 'AD2019110001', 0),
('202004300000009', 'colorful_design', 'assets/img/all_img/', 'img_20200430104521_1.png', '["card_name"]', 'product', '2020-04-30 10:45:21', 'AD2019110001', 0),
('202004300000010', 'comfort_design', 'assets/img/all_img/', 'img_20200430104521_2.png', '["card_name"]', 'product', '2020-04-30 10:45:21', 'AD2019110001', 0),
('202004300000011', 'comfort_design', 'assets/img/all_img/', 'img_20200430104521_3.png', '["card_name"]', 'product', '2020-04-30 10:45:21', 'AD2019110001', 0),
('202004300000012', 'aplikasi_inventori', 'assets/img/all_img/', 'img_20200430105111_0.png', '["web_app"]', 'product', '2020-04-30 10:51:11', 'AD2019110001', 0),
('202004300000013', 'android_app', 'assets/img/all_img/', 'img_20200430105111_1.png', '[]', 'product', '2020-04-30 10:51:11', 'AD2019110001', 0),
('202004300000014', 'sikecil_app', 'assets/img/all_img/', 'img_20200430105111_2.png', '["web_app"]', 'product', '2020-04-30 10:51:11', 'AD2019110001', 0),
('202005010000001', 'for_article', 'assets/img/all_img/', 'img_20200501051952_0.png', '["grand opening"]', 'product', '2020-05-01 05:19:52', 'AD2019110001', 0),
('202005010000002', 'logo_filcod', 'assets/img/all_img/', 'img_20200501055623_0.png', '["filosoficode"]', 'product', '2020-05-01 05:56:23', 'AD2019110001', 0),
('202005020000001', 'berdikari', 'assets/img/all_img/', 'img_20200502012722_0.png', '[]', 'product', '2020-05-02 01:27:22', 'AD2019110001', 0),
('202005020000002', 'partnership', 'assets/img/all_img/', 'img_20200502013724_0.png', '["partnership"]', 'header', '2020-05-02 01:37:24', 'AD2019110001', 0),
('202005020000003', 'ldr', 'assets/img/all_img/', 'img_20200502025633_0.png', '["article"]', 'product', '2020-05-02 02:56:33', 'AD2019110001', 0),
('202005020000004', 'nulis_bareng_pasangan', 'assets/img/all_img/', 'img_20200502032730_0.png', '["article"]', 'product', '2020-05-02 03:27:30', 'AD2019110001', 0),
('202005020000005', 'bisnis_bareng_pasangan', 'assets/img/all_img/', 'img_20200502032730_1.png', '["article"]', 'product', '2020-05-02 03:27:30', 'AD2019110001', 0),
('202005020000006', 'cash_back', 'assets/img/all_img/', 'img_20200502034328_0.png', '["article"]', 'product', '2020-05-02 03:43:28', 'AD2019110001', 0),
('202005020000007', 'gejala_corona', 'assets/img/all_img/', 'img_20200502094140_0.png', '["article"]', 'product', '2020-05-02 09:41:40', 'AD2019110001', 0),
('202005020000008', 'stay_at_home', 'assets/img/all_img/', 'img_20200502094402_0.png', '["article"]', 'product', '2020-05-02 09:44:02', 'AD2019110001', 0),
('202005020000009', 'corona', 'assets/img/all_img/', 'img_20200502094707_0.png', '["article"]', 'product', '2020-05-02 09:47:07', 'AD2019110001', 0),
('202005020000010', 'surya', 'assets/img/all_img/', 'img_20200502112141_2.png', '["profil"]', 'profil', '2020-05-02 11:21:41', 'AD2019110001', 0),
('202005020000011', 'babi_got', 'assets/img/all_img/', 'img_20200502112141_3.png', '["profil"]', 'profil', '2020-05-02 11:21:41', 'AD2019110001', 0),
('202005020000012', 'mey', 'assets/img/all_img/', 'img_20200502112141_4.png', '["profil"]', 'profil', '2020-05-02 11:21:41', 'AD2019110001', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_img_category`
--

CREATE TABLE IF NOT EXISTS `m_img_category` (
  `id_img_ct` varchar(32) NOT NULL,
  `ct` text NOT NULL,
  `id_img_ct_base` varchar(32) NOT NULL,
  PRIMARY KEY (`id_img_ct`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_img_category`
--

INSERT INTO `m_img_category` (`id_img_ct`, `ct`, `id_img_ct_base`) VALUES
('202004160000016', 'filosofi_code promo', ''),
('202004160000017', 'grand opening', ''),
('202004160000018', 'work from home', ''),
('202004160000019', 'stay at home', ''),
('202004160000020', 'ends collection', ''),
('202004160000021', 'long dress', ''),
('202004160000022', 'daster', ''),
('202004300000023', 'card_name', ''),
('202004300000024', 'web_app', ''),
('202004300000025', 'android_app', ''),
('202005010000026', 'filosoficode', ''),
('202005020000027', 'article', ''),
('202005020000028', 'partnership', ''),
('202005020000029', 'profil', '');

--
-- Trigger `m_img_category`
--
DROP TRIGGER IF EXISTS `before_insert_img_cate`;
DELIMITER //
CREATE TRIGGER `before_insert_img_cate` BEFORE INSERT ON `m_img_category`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'm_img_category';
    
    SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'm_img_category';
    
    SET fix_key_user = concat(left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_img_ct = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id_product` varchar(32) NOT NULL,
  `nama_product` varchar(64) NOT NULL,
  `id_toko` varchar(32) NOT NULL,
  `id_brand` varchar(32) NOT NULL,
  `category_produk` text NOT NULL,
  `desc_product` text NOT NULL,
  `spec_product` text NOT NULL,
  `img_list_product` text NOT NULL,
  `tag_product` text NOT NULL,
  `price_product` int(64) NOT NULL,
  `disc_product` varchar(64) NOT NULL,
  `sts_pr_product` enum('0','1') NOT NULL,
  `sts_nego_product` enum('0','1') NOT NULL,
  `is_delete_product` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id_product`, `nama_product`, `id_toko`, `id_brand`, `category_produk`, `desc_product`, `spec_product`, `img_list_product`, `tag_product`, `price_product`, `disc_product`, `sts_pr_product`, `sts_nego_product`, `is_delete_product`) VALUES
('PRD202004230000011', 'Aplikasi Inventori dan Pencatatan Keuangan', 'TK202004010000005', 'filosofi_code', '["Website Application","Website Design"]', '<p>Aplikasi Ini Baik</p>', '', '["base_url/assets/img/all_img/img_20200430105111_0.png"]', '["#aplikasibagus","#aplikasibaik","#aplikasimurah","#aplikasigampang","#promoaplikasi"]', 0, '100000', '1', '1', '0'),
('PRD202004230000012', 'Aplikasi Android Mini Akuntansi UMKM', 'TK202004010000005', 'filosofi_code', '["Website Application","UI UX Design","Design Android Application"]', '<p>Aplikasi Sikecil</p>', '', '["base_url/assets/img/all_img/img_20200430105111_1.png"]', '[]', 0, '0', '1', '1', '0'),
('PRD202004230000013', 'Website Admin Aplikasi Mini Akuntansi UMKM', 'TK202004010000005', 'filosofi_code', '["Website Application","Website Design","UI UX Design"]', '<p>Website Admin Aplikasi Mini Akuntansi UMKM<br></p>', '', '["base_url/assets/img/all_img/img_20200430105111_0.png"]', '[]', 0, '0', '1', '1', '0'),
('PRD202004230000014', 'NEO Green Card Name', 'TK202004010000005', 'filosofi_code', '["Design Lainnya"]', '<p>Kartu Nama Vintage<br></p>', '', '["base_url/assets/img/all_img/img_20200430103853_0.png","base_url/assets/img/all_img/img_20200430103853_1.png"]', '["#beautifulcardname","#cardname","#desainkartunama","#cardnamedesign","#NEOdesign","#design"]', 100000, '10000', '0', '1', '0'),
('PRD202004300000017', 'sky blue card name', 'TK202004010000005', 'filosofi_code', '["Design Lainnya"]', '<p>sky blue card name<br></p>', '', '["base_url/assets/img/all_img/img_20200430104243_0.png","base_url/assets/img/all_img/img_20200430104243_1.png"]', '[]', 100000, '10000', '0', '1', '0'),
('PRD202004300000018', 'modern cardname', 'TK202004010000005', 'filosofi_code', '["Design Lainnya"]', '<p>modern cardname<br></p>', '', '["base_url/assets/img/all_img/img_20200430104521_2.png","base_url/assets/img/all_img/img_20200430104521_3.png"]', '[]', 100000, '10000', '0', '1', '0');

--
-- Trigger `product`
--
DROP TRIGGER IF EXISTS `before_insert_product`;
DELIMITER //
CREATE TRIGGER `before_insert_product` BEFORE INSERT ON `product`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'product';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'product';
    
    SET fix_key_user = concat("PRD",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_product = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_brand`
--

CREATE TABLE IF NOT EXISTS `product_brand` (
  `id_brand` varchar(32) NOT NULL,
  `nama_brand` varchar(64) NOT NULL,
  `is_delete_brand` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_brand`
--

INSERT INTO `product_brand` (`id_brand`, `nama_brand`, `is_delete_brand`) VALUES
('BRD2020041900017', 'filosofi_code', '0'),
('BRD2020041900018', 'ends collection', '0');

--
-- Trigger `product_brand`
--
DROP TRIGGER IF EXISTS `before_insert_brand`;
DELIMITER //
CREATE TRIGGER `before_insert_brand` BEFORE INSERT ON `product_brand`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'brand';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'brand';
    
    SET fix_key_user = concat("BRD",left(NOW()+0, 8),"",LPAD(rowcount_next, 5, '0'));
    
    SET NEW.id_brand = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_jenis`
--

CREATE TABLE IF NOT EXISTS `product_jenis` (
  `id_jenis` varchar(32) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL,
  `parent_id` varchar(32) NOT NULL,
  `is_delete_jenis` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_jenis`
--

INSERT INTO `product_jenis` (`id_jenis`, `nama_jenis`, `parent_id`, `is_delete_jenis`) VALUES
('PJ2020041900019', 'Website Application', '0', '0'),
('PJ2020041900020', 'Website Design', '0', '0'),
('PJ2020041900021', 'UI UX Design', '0', '0'),
('PJ2020041900022', 'Design Lainnya', '0', '0'),
('PJ2020041900023', 'Design Android Application', '0', '0'),
('PJ2020041900024', 'Android Application', '0', '0'),
('PJ2020041900025', 'Photograpy', '0', '0'),
('PJ2020041900026', 'Video Peomotion', '0', '0'),
('PJ2020041900027', 'Video Animation', '0', '0'),
('PJ2020041900028', '3D Animation', '0', '0');

--
-- Trigger `product_jenis`
--
DROP TRIGGER IF EXISTS `before_insert_jenis`;
DELIMITER //
CREATE TRIGGER `before_insert_jenis` BEFORE INSERT ON `product_jenis`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'product_jenis';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'product_jenis';
    
    SET fix_key_user = concat("PJ",left(NOW()+0, 8),"",LPAD(rowcount_next, 5, '0'));
    
    SET NEW.id_jenis = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pr_article`
--

CREATE TABLE IF NOT EXISTS `pr_article` (
  `id_pr` int(11) NOT NULL AUTO_INCREMENT,
  `id_article_pr` varchar(32) NOT NULL,
  `tgl_add_pr` datetime NOT NULL,
  `add_by_pr` varchar(32) NOT NULL,
  `active_pr` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_pr`,`id_article_pr`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `pr_article`
--

INSERT INTO `pr_article` (`id_pr`, `id_article_pr`, `tgl_add_pr`, `add_by_pr`, `active_pr`) VALUES
(2, 'ARC202004160000000021', '2020-04-17 14:31:18', 'AD2019110001', '1'),
(3, 'ARC202004160000000022', '2020-04-17 14:31:22', 'AD2019110001', '1'),
(4, 'ARC202004160000000023', '2020-04-17 14:31:25', 'AD2019110001', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pr_page_home`
--

CREATE TABLE IF NOT EXISTS `pr_page_home` (
  `id_pr` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis` varchar(32) NOT NULL,
  `align_header_pr` varchar(32) NOT NULL,
  `r_num_pr` int(11) NOT NULL,
  `tgl_add_pr` datetime NOT NULL,
  `add_by_pr` varchar(32) NOT NULL,
  `sts_pr` enum('0','1') NOT NULL,
  `active_pr` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_pr`,`id_jenis`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `pr_page_home`
--

INSERT INTO `pr_page_home` (`id_pr`, `id_jenis`, `align_header_pr`, `r_num_pr`, `tgl_add_pr`, `add_by_pr`, `sts_pr`, `active_pr`) VALUES
(1, 'PJ2020041900019', 'left', 1, '2020-04-21 15:23:03', 'AD2019110001', '1', '1'),
(2, 'PJ2020041900020', 'right', 2, '2020-04-21 15:27:19', 'AD2019110001', '1', '1'),
(3, 'PJ2020041900021', 'left', 0, '2020-04-21 15:27:46', 'AD2019110001', '0', '1'),
(4, 'PJ2020041900022', 'right', 4, '2020-04-21 15:27:50', 'AD2019110001', '1', '1'),
(5, 'PJ2020041900023', 'left', 3, '2020-04-21 15:27:53', 'AD2019110001', '1', '1'),
(7, 'PJ2020041900024', 'right', 6, '2020-04-21 16:06:11', 'AD2019110001', '0', '1'),
(8, 'PJ2020041900025', 'left', 5, '2020-04-22 20:10:31', 'AD2019110001', '1', '1'),
(9, 'PJ2020041900026', 'left', 0, '2020-04-22 20:52:30', 'AD2019110001', '0', '1'),
(10, 'PJ2020041900027', 'left', 0, '2020-04-22 20:52:34', 'AD2019110001', '0', '1'),
(11, 'PJ2020041900028', 'left', 0, '2020-04-22 20:52:37', 'AD2019110001', '0', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `toko`
--

CREATE TABLE IF NOT EXISTS `toko` (
  `id_toko` varchar(32) NOT NULL,
  `tipe_owner` varchar(32) NOT NULL,
  `id_owner` varchar(20) NOT NULL,
  `nama_toko` text NOT NULL,
  `desc_toko` text NOT NULL,
  `main_img_toko` text NOT NULL,
  `img_list_toko` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_toko`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `toko`
--

INSERT INTO `toko` (`id_toko`, `tipe_owner`, `id_owner`, `nama_toko`, `desc_toko`, `main_img_toko`, `img_list_toko`, `is_delete`) VALUES
('TK202004010000005', 'user', 'surya', 'Filosofi_code', '<p>Toko terbaik sepanjang hidup saya adalah Filosofi_code</p>', 'base_url/assets/img/all_img/img_20200417111835_4.png', '["base_url/assets/img/all_img/img_20200326085740_0.png","base_url/assets/img/all_img/img_20200326090127_0.png","base_url/assets/img/all_img/img_20200326090127_0.png","base_url/assets/img/all_img/img_20200326090127_0.png","base_url/assets/img/all_img/img_20200326090127_0.png"]', '0'),
('TK202004010000006', 'user', 'surya', 'Surya Gemilang', '<p>Toko Pedia</p>', 'base_url/assets/img/all_img/img_20200417111835_0.png', '["base_url/assets/img/all_img/img_20200326090127_1.png","base_url/assets/img/all_img/img_20200326090127_1.png","base_url/assets/img/all_img/img_20200326090127_1.png","base_url/assets/img/all_img/img_20200326085740_0.png","base_url/assets/img/all_img/img_20200326085740_0.png"]', '0'),
('TK202004280000007', 'user', 'surya', 'Admin Gumilang Mall', '<p><img src="base_url/assets/img/all_img/img_20200423124632_1.png">bagus ya mas ok lah ini seru loh bagus ya mas ok lah ini seru lohvbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru lohbagus ya mas ok lah ini seru loh<br></p>', 'base_url/assets/img/all_img/img_20200423124303_0.png', '["base_url/assets/img/all_img/img_20200417111835_0.png","base_url/assets/img/all_img/img_20200423124632_1.png","base_url/assets/img/all_img/img_20200423124632_1.png","base_url/assets/img/all_img/img_20200423124632_1.png","base_url/assets/img/all_img/img_20200423124632_1.png"]', '1');

--
-- Trigger `toko`
--
DROP TRIGGER IF EXISTS `before_insert_toko`;
DELIMITER //
CREATE TRIGGER `before_insert_toko` BEFORE INSERT ON `toko`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index 
    INTO rowcount
    FROM index_number where id = 'toko';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'toko';
    
    SET fix_key_user = concat("TK",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_toko = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` varchar(15) NOT NULL,
  `id_tipe_user` varchar(3) NOT NULL,
  `email_user` text NOT NULL,
  `username_user` varchar(32) NOT NULL,
  `tlp_user` varchar(13) NOT NULL,
  `nama_user` varchar(64) NOT NULL,
  `alamat_user` text NOT NULL,
  `pass_user` text NOT NULL,
  `status_active_user` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `id_tipe_user`, `email_user`, `username_user`, `tlp_user`, `nama_user`, `alamat_user`, `pass_user`, `status_active_user`, `is_delete`) VALUES
('202003220000001', '0', 'suryahanggara@gmail.com', 'surya', '081230695774', 'surya hanggara', 'malang', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb', '1', '0'),
('202003220000002', '0', 'suryahanggarax@gmail.com', 'suryax', '081230695771', 'surya', 'malang', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb', '1', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
